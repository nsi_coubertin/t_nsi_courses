___
## Exercices

!!! question "Exercice 1: On considère la table Etudiant qui suit :"
    |numero|nom|inscription|faculte|
    |:-:|:-:|:-:|:-:|
    |20201975|Louis Dors|05/09/2020|sciences|
    |20200811|Tom Eigeri|02/09/2020|droit|
    |20202368|José Parentré|06/09/2020|lettres|
    |20190493|Anne Hémie|02/09/2019|médecine|
    |20201832|Jacques Célair|05/09/2020|staps|
    |20192105|Aubin Sahalor|04/09/2019|sciences|
    |20191128|Thibaud Monfils|03/09/2019|lettres|
    |20200751|Sarah Freichi|02/09/2019|droit|

1. Citez les attributs de cette relation en précisant leur domaine.

2. Le tuple **(20192105, "Thibaud Monfils", 03/09/2019; "sciences")** appartient-il à cette relation ? Justifiez votre réponse.

3. Quel rôle peut-on donner à l'attribut **numero** dans cette relation ?

4. Donnez le schéma relationnel de cette relation.

5. Citer les redondances que l'on trouve dans cette table.

6. A-t-on intérêt à scinder cette table en créant une table pour l'attribut **inscription** ?

7. A-t-on intérêt à scinder cette table en créant une table pour l'attribut **faculte** ?

8. Proposez une autre conception de la base de données en scindant cette table en deux tables **Etudiant** et **Faculte**.

9. Donnez le schéma relationnel de la base de données.

10. Si on supprime le tuple d'attribut **faculté** égal à **sciences** dans la relation **Faculte**, quelles en sont les conséquences ?


!!! question "Exercice 2 : On considère la table Internaute renseignée lors de l'inscription à un site :"

    |nom|naissance|email|pseudo|
    |:-:|:-:|:-:|:-:|
    |Anna Conda|21/01/1990|a.conda@liberte.fr|Croc15|
    |Luc Ratif|14/11/1995|lratif@tropcool.com|Skyrythm|
    |Amandine Aheurfix|05/12/2001|amandix@zone51.org|Ufologue|
    |Marc Assin|18/06/2000|m.assin3@liberte.fr|Quileur0|
    |Béa Bas|09/05/1998|bbas@aloha.net|Sunnyx|
    |Agathe Zeblues|16/02/1992|piano@musique.fr|Piano|
    |Charles Magne|23/04/1997|cmagne2@historia.org|Durandal|
    |Paul Ichinel|12/08/2002|paulic@tropcool.com|Flask34|

1. Indiquez pour chaque attribut s'il peut servir de clé primaire.  
    nom :  
    naissance :  
    email :  
    pseudo :  
    
2. Donnez deux schémas relationnels possibles pour la relation **Internaute**.


!!! question "Exercice 3 :On considère la table suivante qui rassemble les notes sur 10, accordées à différents films par les abonnés au site cine.fr :"

    |id|titre|sortie|nom|internaute|note|
    |:-:|:-:|:-:|:-:|:-:|:-:|
    |1|Idiocracy|2007|Anne Oraque|aoraque@cine.fr|7|
    |2|Avatar|2009|Maud Tete|mtete2@cine.fr|9|
    |3|Minority Report|2002|Eva Poret|eporet@cine.fr|5|
    |4|L'Homme bicentenaire|2002|Guy Bol|gbol1@cine.fr|7|
    |5|Minority Report|2002|Maud Tete|mtete2@cine.fr|8|
    |6|Avatar|2009|Guy Bol|gbol1@cine.fr|10|
    |7|Idiocracy|2007|Eva Poret|eporet@cine.fr|6|
    |8|Minority Report|2002|Alain Di|adi5@cine.fr|4|
    |9|Avatar|2009|Eva Poret|eporet@cine.fr|8|
    |10|Avatar|2009|Anne Oraque|aoraque@cine.fr|3|
    |11|L'Homme bicentenaire|2002|Maud Tete|mtete2@cine.fr|7|
    |12|Idiocracy|2007|Maud Tete|mtete2@cine.fr|9|
    |13|Minority Report|2002|Ray Nette|rnette@cine.fr|4|
    |14|Avatar|2009|Alain Di|adi5@cine.fr|10|
    |15|Idiocracy|2007|Ray Nette|rnette@cine.fr|5|
    |16|L'Homme bicentenaire|2002|Alain Di|adi5@cine.fr|7|

1. En combien de relations peut-on scinder cette table ?

2. Donner le schéma relationnel de cette base de données qui en découle.

3. Donnez le corps des relations qui en découle.
