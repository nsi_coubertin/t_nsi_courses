___
## Introduction

**R.S.A.** correspond aux initiales de ses inventeurs qui l'ont inventé en 1977 :

    Ron Rivest, Adi Shamir et Leonard Adleman.
    
RSA est un algorithme de chiffrement asymétrique qui sert aussi bien à la cryptographie de documents, qu'à l'authentification (signature numérique, code de carte bancaire, ...). 

Parce-qu'il est basé sur une clé publique, et qu'il soit très sûr, l'algorithme RSA est devenu un standard dans le monde.

La clé de chiffrement est publique. Elle est constituée d'un couple de nombres :

    (module de chiffrement, exposant de chiffrement)

La clé de déchiffrement est privée. Elles est aussi formée d'un couple de nombres :

    (module de chiffrement, exposant de déchiffrement)

Les deux clés sont associées et uniques : elles ne peuvent pas être utilisées avec d'autres clés.

Tout le monde peut chiffrer un message avec la clé publique mais seul le détenteur de la clé privée peut le déchiffrer. 

Tout le principe de RSA repose sur le fait qu'il est très difficile et très long de factoriser un très grand nombre en deux facteurs premiers : il n'existe pas d'algorithme connu de la communauté scientifique pour réaliser une attaque force brute avec des ordinateurs classiques.

Il faut donc choisir deux nombres premiers suffisamment grands.  
La taille recommandée pour les clés RSA est 2 048 bits (617 chiffres en base 10).

!!! info "Record actuel"
    En décembre 2019, une équipe de l’Inria à Nancy et du Laboratoire lorrain de recherche en informatique et ses applications (Loria – Inria, CNRS), associée aux universités de Limoges et de San Diego (Californie) , ont "cassé" une clé à 795 bits (240 chiffres).  
    **35 millions d'heures de calcul sur 3 centres ont été nécessaires.**


___
## Fonctions préliminaires

### Etape 1 :  Clé de chiffrement

1. Écrire une fonction `est_premier` qui prend en paramètre un nombre $n$ et permet de préciser si ce nombre est premier :

    !!! done "Attendu"
        ```python
        def est_premier(n) :
            ''' ...
        
        :Examples:
        >>> est_premier(17)
        True
        >>> est_premier(15)
        False
        >>> est_premier(1)
        False
        '''
        ```

2. Écrire une fonction `premier_suivant` qui prend en paramètre un nombre entier $rang$ et renvoie le nombre premier immédiatement supérieur à $rang$

    !!! done "Attendu"
        ```python
        def premier_suivant(rang) :
            ''' ...
        
        :Examples:
        >>> premier_suivant(3)
        5
        >>> premier_suivant(20)
        23
        >>> premier_suivant(32)
        37
        '''
        ```

3. Écrire une fonction `module_chiffrement` qui prend en paramètre deux nombres entiers $p$ et $q$ et renvoie leur produit.

    !!! done "Attendu"
        ```python
        def module_chiffrement(p, q) :
            ''' ...
        
        :Examples:
        >>> module_chiffrement(3, 7)
        21
        '''
        ```

4. Écrire une fonction `valeur_indicatrice_euler` qui prend en paramètre deux nombres entiers $p$ et $q$ et renvoie le produit $(p-1) (q-1)$.

    !!! done "Attendu"
        ```python
        def valeur_indicatrice_euler(n, p) :
            ''' ...
    
        :Examples:
        >>> valeur_indicatrice_euler(3, 7)
        12
        >>> valeur_indicatrice_euler(13, 23)
        264
        '''
        ```

5. Écrire une fonction `liste_nombres_premiers` qui prend en paramètre deux nombres entiers $borne\_inf$ et $borne\_sup$. Elle renvoie la liste des nombres premiers compris dans l'intervelle $[borne\_inf ~;~ borne\_sup]$.

    !!! done "Attendu"
        ```python
        def liste_nombres_premiers(borne_inf, borne_sup) :
            ''' ...
        
        :Examples:
        >>> liste_nombres_premiers(0, 10)
        [2, 3, 5, 7]
        >>> liste_nombres_premiers(20, 40)
        [23, 29, 31, 37]
        '''
        ```


### Etape 2 :   Clé de déchiffrement

Écrire une fonction `solution_diophante` qui prend en paramètre deux nombres entiers, $phi$ et $e$.  
La fonction renvoie la solution $d$ de l'équation suivante :$(e × d) ~mod~ phi = 1$  
Le reste de la division de $ed$ par $phi$ est 1. (_mod = modulo_)

!!! done "Attendu"
    ```python
    def solution_diophante(phi, e) :
        ''' ...
        
    :Examples:
    >>> solution_diophante(220, 17)
    13
    >>> solution_diophante(480, 13)
    37
    '''
    ```


## Création des clés

!!! info "Méthode"
    1. Choisir $p$ et $q$, deux nombres premiers distincts, assez grand

    2. Module de chiffrement :  
    Calculer leur produit $n = p~q$

    3. Valeur de l'indicatrice d'Euler de $n$ :  
    Calculer la valeur :   $φ(n) = (p - 1)(q - 1)$

    4. Exposant de chiffrement :  
    Choisir un nombre premier $e$ strictement inférieur à $φ(n)$ et qui ne possède de facteur commun avec $φ(n)$

    5. Exposant de déchiffrement  :  
    Calculer l'entier naturel $d$, inférieur à $φ(n)$ tel que $d$ est solution de : $e~d ~~ mod~φ(n) = 1$

Le couple $(n, e)$ est la **clé publique** du chiffrement, alors que sa **clé privée** est le couple $(n, d)$.

Par sécurité, les autres nombres nécessaires à l'établissement de ces clés sont alors à effacer et oublier :  les deux nombres premiers $n$ et $p$, à la base de l'algorithme.

Utiliser les fonctions préliminaires pour créer une dernière fonction `cles_RSA`.

   - Cette fonction prend en paramètre deux nombres entiers $borne\_inf$ et $borne\_sup$.
   - Elle renvoie deux clés (sous forme d'un tuple de deux tuples) établies à partir de deux nombres premiers choisis aléatoirement dans l'intervalle $[borne\_inf ~;~ borne\_sup]$
   
_On ne doit pas connaître les 2 nombres premiers utilisés, ni leur produit._


___
## Utilisation du chiffrement RSA

Le chiffrement RSA permet de transformer un nombre $x$ strictement plus petit que $n$ en un autre nombre $y$ , lui aussi strictement plus petit que $n$, en utilisant la clé de chiffrement $(n, e)$.

$$x \rightarrow\ y ~=~ x^e~~mod~n$$

Écrire une fonction `fonction_RSA` qui prend en paramètre un nombre entier $x$ et une clé de chiffrement RSA de type tuple $(n,e)$.  
Cette fonction renvoie la valeur  $x^e ~~mod~n$.

!!! done "Attendu"
    ```python
    def fonction_RSA(x, cle) :
        '''
        ...
        
    :Examples:
    >>> fonction_RSA(82, (253, 17))
    190
    >>> fonction_RSA(65, (8907,13))
    2102
    '''
    ```

___
## Chiffrement d'un message

Pour crypter un texte, on applique le chiffrement sur l'encodage UTF-8 des caractères utilisés.  
Si on prend un caractère à la fois, cela revient tout simplement à une substitution classique (donc facilement déchiffrable). L'idée est donc de grouper les valeurs d'encodages en formant des blocs de même longueur.

!!! example "Exemple"
    Texte en clair : "1 million €"

    ![tableau 1](images/tableau1.png)
    
    On assemble les codes :  
    490032010901050108010801050111011000328364

    On redivise par blocs de 3, en partant de la fin :  
    490 032 010 901 050 108 010 801 050 111 011 000 328 364

    Après chiffrement avec la clé (2631,29), les valeurs deviennent :

    ![tableau 2](images/tableau2.png)

    Texte chiffré : '1705 1613 2590 1186 1628 1791 2590 795 1628 1287 869 0 2092 2377'

Écrire une fonction `chiffrement_RSA` qui prend en paramètre un `texte` et une clé de chiffrement RSA de type tuple.

!!! done "Attendu"
    ```python
    def chiffrement_RSA(texte, cle) :
    ''' ...
    :Examples:
    >>> chiffrement_RSA('1 million €', (2631, 29))
    '1705 1613 2590 1186 1628 1791 2590 795 1628 1287 869 0 2092 2377'
    >>> chiffrement_RSA('NSI 2020', (316307,44503))
    '73703 165375 147077 279034 283619 20211 244719 10149 297610 119422'
    '''
    ```


## Déchiffrement du message

A l'inverse, le déchiffrement permet de transformer un nombre $y$ strictement plus petit que $n$ en un autre nombre $x$, lui aussi strictement plus petit que  $n$, en utilisant la clé de déchiffrement $(n, d)$.

$$y \rightarrow\ x ~=~ y^d~~mod~n$$

Bien que le chiffrement RSA soit asymétrique (clés différentes), la fonction `fonction_RSA` permet également le déchiffrement.


Écrire une fonction `dechiffrement_RSA` qui prend en paramètre un `texte_chiffre` et une clé de chiffrement RSA de type tuple.

!!! done "Attendu"
    ```python
    def dechiffrement_RSA(texte_chiffre, cle) :
        ''' ...

    :Examples:
    >>> dechiffrement_RSA('73703 165375 147077 279034 283619 20211 244719 10149     297610 119422', (316307, 7))
    'NSI 2020'
    >>> dechiffrement_RSA('457 1 8159 3533', (8907, 3653))
    'Fin'
    '''
    ```
