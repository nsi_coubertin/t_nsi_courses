___
## Exercices

!!! question "Exercice 1"
    Créez une nouvelle base de données **`Client.db`** dans `DB Browser for SQLite` et exécutez les requêtes suivantes pour l'initialiser.

    ```sql
    CREATE TABLE "Client" (
	    "numero"	INTEGER NOT NULL,
	    "prenom"	VARCHAR(32) NOT NULL,
	    "nom"	VARCHAR(32) NOT NULL,
	    "adresse"	VARCHAR(128) NOT NULL,
	    "codepostal"	INTEGER NOT NULL,
	    "ville"	VARCHAR(32) NOT NULL,
	    "categorie"	VARCHAR(32) NOT NULL,
	    PRIMARY KEY("numero")
    );

    INSERT INTO "Client" VALUES(1423,"Albert","Muda","3 rue des pins",35120,"Epiniac","professionnel");
    INSERT INTO "Client" VALUES(2475,"Cécile","Onxa","20 rue du mont",57100,"Manom","particulier");
    INSERT INTO "Client" VALUES(4120,"Ella","Dubol","58 rue du soleil",30500,"Courry","particulier");
    INSERT INTO "Client" VALUES(2618,"Anne","Onime","12 rue des lys",25310,"Blamont","particulier");
    INSERT INTO "Client" VALUES(2041,"Clément","Tine","12 rue du café",28600,"Luisant","professionnel");
    INSERT INTO "Client" VALUES(1284,"André","Zienne","26 rue du sable",45800,"Combleux","particulier");
    INSERT INTO "Client" VALUES(1972,"Denis","Douillet","32 rue du verre",23500,"Croze","particulier");
    INSERT INTO "Client" VALUES(1739,"Guy","Don","2 rue de la lune",23400,"Auriat","professionnel");
    INSERT INTO "Client" VALUES(1756,"Anna","Logis","47 rue des champs",23800,"Sagnat","particulier");
    INSERT INTO "Client" VALUES(2847,"Frank","Hontoi","35 rue des ertes",53200,"Daon","particulier");
    INSERT INTO "Client" VALUES(1337,"Donna","Trice","16 rue des glaces",84100,"Orange","particulier");
    INSERT INTO "Client" VALUES(3905,"Ada","Corre","6 rue du lac",61200,"Fleuré","professionnel");
    ```

1. Combien de colonnes trouve-t-on dans la table **Client** ?

2. Combien de lignes trouve-t-on dans la table **Client** ?
       
3. Quel est le nom de l'attribut correspondant à la clé primaire ?	
       
4. Décrire les tables obtenues en exécutant les requêtes SQL suivantes :
       
    a) ```sql
       SELECT ville FROM Client;
       ```

    b) ```sql
       SELECT * FROM Client WHERE categorie="particulier";
       ```
           
5. Donner les requêtes SQL permettant d'obtenir les tables suivantes :
       
    a) La table donnant l'adresse, le code postal et la ville de chaque client.

    b) La table donnant le numéro, le prénom et le nom des clients professionnels.

    c) La table complète des clients vivant à Orange.

    d) La table complète des clients vivant dans la Creuze (département 23).

    e) La table complète des clients dont le prénom contient la lettre a.



!!! question "Exercice 2"
    On considère la table **Scientifique** :

    |id|prenom|nom|naissance|mort|sexe|specialite|
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    |1|Frances|Allen|1932-08-04|2020-08-04|F|informatique|
    |2|Charles|Babbage|1791-12-26|1871-10-18|H|informatique|
    |3|Edgar Frank|Codd|1923-08-23|2003-04-18|H|informatique|
    |4|Marie|Curie|1867-11-07|1934-07-04|F|physique|
    |5|Charles|Darwin|1809-02-12|1882-04-19|H|biologie|
    |6|Albert|Einstein|1879-03-14|1955-04-18|H|physique|
    |7|Dian|Fossey|1932-01-16|1985-12-26|F|biologie|
    |8|Evariste|Galois|1811-10-25|1832-05-31|H|mathematique|
    |9|Carl Friedrich|Gauss|1777-04-30|1855-02-23|H|mathematique|
    |10|Sophie|Germain|1776-04-01|1831-06-27|F|mathematique|
    |11|Grace|Hopper|1906-12-09|1992-01-01|F|informatique|
    |12|Ada|Lovelace|1815-12-10|1852-11-27|F|informatique|
    |13|Isaac|Newton|1643-01-04|1727-03-31|H|physique|
    |14|Emmy|Noether|1882-03-23|1935-04-14|F|mathematique|
    |15|Louis|Pasteur|1822-12-27|1895-09-28|H|biologie|
    |16|Vera|Rubin|1928-07-23|2016-12-25|F|physique|

Quelles requêtes SQL faut-il formuler pour obtenir les tables suivantes ?

1. La table formée par le prénom et le nom de chaque scientifique.

2. La table contenant les différentes spécialités mais sans répétition.

3. La table contenant les noms des femmes scientifiques uniquement.

4. La table contenant les noms et prénoms des spécialistes en mathématique et des spécialistes en informatique.

5. La table complète des scientifiques dont le nom commence par la lettre G.

6. La table complète des scientifiques nés au XXème siècle.

7. La table complète des scientifiques nés entre début 1810 et fin 1820.

8. La table complète des mathématiciens (hommes) ayant vécu au XIXème siècle.


!!! question "Exercice 3"
    On souhaite apporter quelques modifications à la table **Scientifique** de l'exercice précédent sachant qu'elle a été créée sur la base du schéma relationnel suivant:"

    ```sql
    Scientifique(id:ENTIER, prenom:CARACTERES(32), nom:CARACTERES(32),  
                 naissance:DATE, mort:DATE, sexe:CARACTERES(1),  
                 specialite:CARACTERES(32))
    ```

1. Quelles requêtes SQL faut-il formuler pour insérer les scientifiques suivants dans table ?

    a) Le fondateur de la science informatique Alan Turing né le 23 juin 1912 et mort le 7 juin 1954.
      
    b) Le physicien Max Planck né le 23 avril 1858 et mort le 4 octobre 1947.
      
    c) La pionnière de la biologie moléculaire Rosalind Franklin née le 25 juillet 1920 et morte le 16 avril 1958.

2. Quelle requête SQL faut-il formuler pour modifier la spécialité "mathematique" en "mathématique" (avec l'accent) ?

3. Une confusion entre le mathématicien Henri Poincaré (né le 29 avril 1854 et mort le 17 juillet 1912) et son cousin Raymond Poincaré a conduit à l'insertion de l'enregistrement suivant :  
        ```sql
        (20, "Raymond", "Poincaré", "1860-08-20", "1934-10-15", "H", "président")
        ```
       
    Proposez deux solutions, sous forme de requêtes SQL, pour corriger cette erreur.

4. Proposez deux requêtes SQL permettant de ne conserver dans la base que les scientifiques dont la spécialité est soit la mathématique soit l'informatique.



!!! question "Exercice 4"
    La table suivante rassemble quelques informations sur des séries télévisées : le titre, l'année de la première diffusion, le nombre de saisons, le pays de production ainsi que la moyenne sur 5 des notes octroyées par les spectateurs.

    |id|titre|diffusion|saisons|production|note|
    |:-:|:-:|:-:|:-:|:-:|:-:|
    |1|Chapeau melon et bottes de cuir|1961|6|GB|3.0|
    |2|Doctor Who|2005|12|GB|4.1|
    |3|Dr House|2004|8|US|4.3|
    |4|Flashpoint|2008|5|CA|3.3|
    |5|FX, Effets Spéciaux|1996|2|CA|2.0|
    |6|Le prisonnier|1967|1|GB|3.2|
    |7|Les Brigades du Tigre|1974|6|FR|2.3|
    |8|Les Chevaliers du ciel|1967|3|FR|3.4|
    |9|Les Enquêtes de Murdoch|2008|14|CA|4.1|
    |10|MacGyver|1985|7|US|3.2|
    |11|Magnum|1980|8|US|2.5|
    |12|Sliders, les mondes parallèles1995||5|US|2.9|
    |13|StarTrek|1966|3|US|2.7|
    |14|Stargate SG1|1997|10|US|3.5|
    |15|The Sentinel|1996|4|CA|2.1|
    |16|X-Files|1993|11|US|2.9|

On décide de créer une base de données pour y stocker cette table. Le schéma relationnel retenu est le suivant :

```sql
Serie(id:ENTIER, titre:TEXTE, diffusion:ANNEE, saisons:ENTIER, 	
        production:CARACTERES(2), note:FLOTTANT)
```

Commencez par créer le fichier « **Serie.db** » dans DB Browser for SQLite puis exécutez le code SQL suivant permettant de créer la table **Serie**.

```sql
CREATE TABLE "Serie" (
	"id"		INTEGER NOT NULL,
	"titre"	TEXT NOT NULL,
	"diffusion"	YEAR NOT NULL,
	"saisons"	INTEGER NOT NULL,
	"production"	CHAR(2) NOT NULL,
	"note"	FLOAT,
	PRIMARY KEY("id")
);
```

1. Quelle est la requête SQL permettant l'insertion de la ligne 2 dans la table **Serie** ?

2. Donner les requêtes SQL permettant d'obtenir les tables suivantes :
    
    a) La table des titres des séries françaises.

    b) La table des titres des séries diffusées la première fois après 2004.

    c) La table complète des séries comportant au plus 3 saisons.

    d) La table des titres des séries ayant une moyenne comprise entre 2 et 3.

    e) La table complète des séries produites aux US qui comportent au moins 10 saisons.

    f) La table complète des séries non américaines diffusées avant 1990.


3. Donner la requête SQL permettant d'insérer la série japonaise « Dragon Ball Z » diffusée pour la première fois en 1989, en déclarant une seule saison et la note de 3,6.

4. Donner la requête SQL permettant de supprimer les séries diffusées avant 1970.

5. Donner la requête SQL permettant de corriger le titre de la série « Stargate SG-1 ».


!!! question "Exercice 5 : Voici une base de données constituée de la table **Fruit** et de la table **Couleur**."

    |id\_fruit|nom|id_couleur||id\_couleur|teinte|code|
    |:-:|:-:|:-:|:-:|:-:|:-:|:-:|
    |1|banane|4||1|rouge|#ff0000|
    |2|cerise|1||2|vert|#00ff00|
    |3|citron|4||3|bleu|#0000ff|
    |4|clémentine|5||4|jaune|#ffff00|
    |5|fraise|1||5|orange|#ffa500|
    |6|kiwi|2|||||
    |7|mandarine|5|||||
    |8|melon|2|||||
    |9|myrtille|3|||||
    |10|orange|5|||||

1. Donner le schéma relationnel de cette base de données.

2. Donner les requêtes SQL permettant d'obtenir les tables suivantes :
    
    a) La table des noms et teintes de différents fruits.

    b) La table des noms de fruits de couleur orange.

    c) La table des noms et teintes de fruits dont le code couleur est **#ff0000**.

    d) La table des noms rassemblant les fruits jaunes et les fruits verts.

    e) La table de la teinte des fraises.

    f) La table des teintes et des noms de fruits commençant par la lettre **m**.

    g) La table des teintes et des noms de fruits commençant par **c** et finissant par **e**.

3. Quelle requête SQL faut-il formuler pour insérer la poire verte ?

4. Quelles requêtes SQL faut-il formuler pour insérer le litchi rose (#FFC0CB) ?

5. Quelles requêtes faut-il formuler pour supprimer la couleur bleu ?  
       (On se place dans la situation où :    PRAGMA foreign_keys = on;)


!!! question "Exercice 6 : Voici une base de données d’œuvres artistiques constituée de trois tables."

    ![base](images/ex6_base.png)

1. Que représentent les attributs **id_categorie** et **id_artiste** dans la relation Oeuvre ?

2. Donner les requêtes SQL permettant d'obtenir les tables suivantes :

    a) La table des titres des dessins.

    b) La table complète des œuvres de Salvador Dali.

    c) La table complète des œuvres de Pablo Picasso créés avant 1940.

    d) La table des titres et année de création des peintures contenant le mot « la ».

    e) La table des titres de sculptures et des noms de leur créateur.

    f) La table des titres des peintures réalisées par les artistes nés avant 1900.

