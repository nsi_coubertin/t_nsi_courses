___
## Exercices

!!! question "Exercice 1 :  Multiplication à la méthode russe"

La méthode du paysan russe est un très vieil algorithme de multiplication de 2 nombres entiers, déjà écrit, sous une autre forme, sur un papyrus égyptien vers 1650 av J.C.  
C'était la principale méthode de calcul en Europe avant l’arrivée des chiffres arabes.

Les premiers ordinateurs l’utilisaient avant que la multiplication ne soit intégrée dans le processeur sous forme de circuit électronique.

Voici son principe écrit sous forme itérative :

    Fonction multiplication (x,y) :
    
	    p <- 0
    	
    	TANT QUE x > 0 :
	    
	    	Si x est impair :
	    		p <- p + y
	    
	    	x <- x // 2
	    
	    	y <- y + y
	    
	    RENVOYER p

1. Appliquer cette fonction pour effectuer le calcul 33 x 153, en détaillant les étapes :

    |x|y|p|
    |:-:|:-:|:-:|
    ||||
    ||||
    ||||
    ||||
    ||||
    ||||
    ||||

2. Écrire cette fonction en langage Python

3. Écrire une fonction équivalente en structure récursive.


!!! question "Exercice 2 :   Longueur d’une liste"

Écrire une fonction `longueur_liste` prenant en paramètre une liste et qui renvoie le nombre d’items dans cette liste.

    a) en structure itérative  
    b) en structure récursive
    
    
!!! question "Exercice 3 : Somme des items numériques d’une liste"

Écrire une fonction `somme_liste` prenant en paramètre une liste et qui renvoie la somme des items numériques dans cette liste.  
_(la liste pouvant être composée de plusieurs types de données)_

    a) en structure itérative  
    b) en structure récursive


!!! question "Exercice 4 :   Flocon de Koch"

![flocon](images/flocon.png)

1. Rechercher le code Python permettant d'effectuer cette figure.

2. Analyser ce code étape par étape à l'ordre 3

3. Essayer de coder en version itérative.
