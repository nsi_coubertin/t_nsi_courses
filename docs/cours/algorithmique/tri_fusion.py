# -*- coding: utf-8 -*-
'''
:Titre : Tri par fusion
:Auteur : L. Conoir
:Date : /2020
'''


def compare(element1, element2) :
    '''La fonction précise si element1 et element2 sont rangés dans l'ordre croissant (ou alphabétique).
:param: element1, element2, type int() ou float() ou str() ou bool()
:return: type bool(), True si l'ordre est respecté, False si ce n'est pas le cas.
:CU: element1 et element2 sont du même type
:bord_effect: None
:examples:
>>> compare(12, 15)
True
>>> compare(12, 12)
True
>>> compare(12, 10)
False
'''
    return element1 <= element2


def fusion(liste1, liste2):
    '''Renvoie la fusion des 2 listes en respectant l'ordre.
:param: liste1, liste2, type list, listes d'éléments
:return: type list, la fusion des 2 listes
:CU: les paramètres sont du type list
:bord_effect: les listes sont modifiées
:examples:
>>> fusion([], [])
[]
>>> fusion([], [2, 4])
[2, 4]
>>> fusion([5, 8], [])
[5, 8]
>>> fusion([5, 8], [2, 4])
[2, 4, 5, 8]'''

    assert (isinstance(liste1, list) and isinstance(liste2, list)), 'Les paramètres ne sont pas de type list.'
    
    liste_fusion = []
    
    while liste1 != [] and liste2!=[] :
    
        if compare(liste1[0], liste2[0]):
            liste_fusion.append(liste1.pop(0))
        else :
            liste_fusion.append(liste2.pop(0))
            
    return liste_fusion + liste1 + liste2


def fusion_recursive(liste1, liste2):
    
    if liste1 == [] :
        return liste2
    
    if liste2 == [] :
        return liste1
    
    if liste1[0] <= liste2[0] :
        return [liste1[0]] + fusion_recursive(liste1[1:], liste2)
    else :
        return [liste2[0]] + fusion_recursive(liste1, liste2[1:])


def tri_fusion(liste):
    '''Trie la liste placée en paramètre par la méthode du tri par fusion.
:param: liste, type list, liste d'éléments
:return: None
:CU: le paramètre est du type list
:bord_effect: None
:examples:
>>> tri_fusion([])
[]
>>> tri_fusion([2])
[2]
>>> tri_fusion([5, 7, 1, 2, 5, 7, 1])
[1, 1, 2, 5, 5, 7, 7]'''

    assert isinstance(liste, list), "Le paramètre n'est pas type list."
    
    milieu = len(liste) // 2
    
    if milieu == 0:
        return liste
    else :
        liste1 = liste[:milieu]
        liste2 = liste[milieu:]
        
        return fusion(tri_fusion(liste1), tri_fusion(liste2))


####################################################################
if __name__ == '__main__' :
    import doctest
    doctest.testmod()