# -*- coding: utf-8 -*-
'''
:Titre : Tri par fusion
:Auteur : L. Conoir
:Date : /2020
'''



def algo_naif(motif, texte) :
    '''La fonction précise si le motif est dans le texte.
La recherche du motif se fait en déplacant ce motif (caractère par caractère) en dessous de texte.
A chaque déplacement, les caractères du motifs sont comparés un par un à ceux du texte.
:param: motif, texte, type str
:return: type boolean
:CU: motif et texte sont de type str, motif est plus petit que texte
:examples:
>>> algo_naif('tent', 'Le chat est content maintenant.')
[15]
>>> algo_naif('en', 'Le chat est content maintenant.')
[16, 25]

'''    
    n = len(texte)
    p = len(motif)
    liste_positions = []

    for indice_texte in range(n - p + 1):
        
        indice_motif = 0
        
        while indice_motif < p and texte[indice_texte + indice_motif] == motif[indice_motif] :
            indice_motif = indice_motif + 1
        
        if indice_motif == p :
            liste_positions.append(indice_texte)        
            
    return liste_positions
            


def pretraitement(chaine):
    '''La fonction recherche dans la chaine l'indice de la dernière occurence
de chaque caractère dans la chaine, sans prendre en compte le dernier.
Le résultat est renvoyé sous forme d'un dictionnaire dans lequel :
   - clé : caractère
   - valeur : l'indice de la dernière occurence du caractère dans la chaine

:param: chaine, type str
:return: type dict
:CU: None
:bord_effect: None
:examples:
>>> pretraitement('banane')
{'b': 0, 'a': 3, 'n': 4}
>>> pretraitement('sciences')
{'s': 0, 'c': 5, 'i': 2, 'e': 6, 'n': 4}
'''
    dico = {}
      
    for indice in range(len(chaine) - 1):
    
        caractere = chaine[indice]
        dico[caractere] = indice
    
    return dico


def verification(motif, texte, position, longueur):
    '''La fonction précise la présence du motif dans le texte à la position demandée.

:param: motif, texte, type str
        position, longueur, type int
:return: type bool
:CU: None
:bord_effect: None
:examples:
>>> verification('na', 'banane', 2, 2)
True
>>> verification('na', 'banane', 3, 2)
False
'''
    ecart = 0
    while ecart < longueur  and motif[ecart] == texte[position + ecart]:
        ecart = ecart + 1
    return ecart == longueur


def algo_Boyer_Moore_Horspool(motif, texte):
    '''La fonction renvoie la liste des positions (si elles existent) dnas le texte où l'on trouve le motif.

:param: motif, texte, type str
:return: type list
:CU: None
:bord_effect: None
:examples:
>>> algo_Boyer_Moore_Horspool('na', 'banane')
[2]
>>> algo_Boyer_Moore_Horspool('ada', 'abracadabra badam badam banane')
[5, 13, 19]
'''
    dico = pretraitement(motif)
    taille_texte = len(texte)
    taille_motif = len(motif)
    indice_texte = 0
    liste_positions = []
        
    while indice_texte < taille_texte - taille_motif + 1:
        
        if verification(motif, texte, indice_texte, taille_motif) :
            liste_positions.append(indice_texte)
            indice_texte = indice_texte + taille_motif
        else :
            rang = dico.get(texte[indice_texte + taille_motif - 1])
            if rang is None :
                indice_texte = indice_texte + taille_motif
            else :
                indice_texte = indice_texte + taille_motif - 1 - rang
    
    return liste_positions


import timeit
with open('genome.txt', 'r', encoding = 'UTF-8') as fichier :
    genome = fichier.read()
sequence = 'AACATTAGGGAGGA'

time1 = timeit.timeit('algo_naif(sequence, genome)', number = 100, globals=globals())
time2 = timeit.timeit('algo_Boyer_Moore_Horspool(sequence, genome)', number = 100, globals=globals())
print(time2/time1)
    

with open('DelaTerrealaLune.txt', 'r', encoding = 'UTF-8') as fichier :
    roman = fichier.read()
phrase = "c'était leur ami dévoué"

time3 = timeit.timeit('algo_naif(phrase, roman)', number = 10, globals=globals())
time4 = timeit.timeit('algo_Boyer_Moore_Horspool(phrase, roman)', number = 10, globals=globals())
print(time4/time3)

####################################################################
if __name__ == '__main__' :
    import doctest
    doctest.testmod()