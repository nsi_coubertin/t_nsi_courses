___
## La base des bases de données

Une base de données est un **ensemble ordonné de données** dont l'organisation est régie par un **modèle de données**.  
*Les bases de données peuvent être divisées en deux catégories : bases de données relationnelles et non relationnelles (hors programme).*

On agit sur une base de données en utilisant un **Système de Gestion de Base de Données**, **SGBD** (ou DBMS en anglais, DataBase Management System) qui permet :
	
- **le stockage des données**
- **un traitement efficace des requêtes**
- **la gestion des accès concurrents** (connexions multiples)
- **la sécurisation des accès** (droits différents selon les profils d’utilisateurs)

La plupart des SGBDR  utilisent le langage **SQL** : **Structured Query Language.**

??? note "Principaux systèmes"

    === "Open source"
        !!! note inline end "Anecdote"
                Les noms MySQL et Maria SQL viennent de My et Maria, les deux filles du développeur Michael "Monty" Widenius.
        === "MySQL"
            Système lancé en 1995, utilisé principalement par les applications web, de type CMS (content management system), comme Joomla et Wordpress, proposé par la plupart des hébergeurs comme serveur de base de données pour les sites web, utilisé par Facebook, Google, Twitter, YouTube.  
            Propriété d’Oracle depuis 2008, MySQL existe également en version payante.

        === "MariaDB"
            Créé en 2009 par des développeurs de MySQL suite à l’achat de ce dernier par Oracle en 2008. MariaDB est très similaire à MySQL et utilisé par Google, Mozilla, Wikimedia...  

        === "PostgreSQL"
            SGBD utilisé par exemple dans des applications de jeux en ligne, installé par défaut sur les serveurs MacOS. Le projet Postgres a été initié en 1985 par Michael Stonebraker pour remplacer la base de données Ingres, et sa première version a été publiée en 1996 sous le nom PostgreSQL.

        === "SQLite"
            SGBD léger, du domaine public, permet de manipuler localement des bases de données sans passer par un serveur (première version en 2000).

    === "Propriétaires"

        === "Oracle"
            Référence historique, puisque la première version du SGBD Oracle est sortie en 1979. Elle est développée par la société Oracle Corporation, crée en 1977.

        === "Microsoft SQL Server et Microsoft Access"
            Les SGBD de Microsoft. Access est un logiciel livré avec le pack MS Office permettant de manipuler localement les bases de données, comme SQLite, tandis que Microsoft SQL Server est un serveur SQL au même titre qu’Oracle ou MySQL.

La création d'une base de données informatique nécessite plusieurs étapes.

___
## Modélisation

La modélisation d'un projet avec l'appui d'une base de données se réalise en trois étapes principales qui correspondent à trois niveaux d'abstraction :

- **Niveau conceptuel** : description du projet en termes d'objets. Cette description est indépendante de tout support informatique.  
On parle de modèle entité-association. Les entités sont les objets du projet.
- **Niveau logique** : description des liens logiques entre les objets du projet. On parle dans ce cas de modèle relationnel. 
- **Niveau physique** : implémentation informatique sur un SGBD

Un des modèles de données le plus courant est le **modèle relationnel**.  
Les principes de base de ce modèle sont les suivants :

- séparer les données dans plusieurs relations  
	- chaque **relation** contient des données , relatives à un **même type d'objet**  
	- chaque objet possédant les mêmes **attributs** (adjectif qualificatif)  
	- **un attribut** ne contient qu'**une information**  
	- en **évitant la redondance** des données  
	- sans stocker les données qui peuvent être calculées (ex : une ligne Total)  
- mettre **les liens entre les relations** par l'utilisation d'attribut(s) spécifique(s)
	- leur valeur permettent d'identifier distinctement une entité
	- la création d'un attribut arbitraire est parfois nécessaire si aucun ne le permet

Le nom d’une **relation avec ses attributs** (noms et types ou domaines) constitue le **schéma relationnel de la relation**.

**L’ensemble des schémas relationnels forme le schéma relationnel ou la structure de la base de données.**

!!! question "Exemple :"
    Un lycée est constitué de 6 classes de terminales.  
    Dans chaque classe, il y a un certain nombre d'élèves (inférieur à 35).  
    Plusieurs disciplines sont enseignées mais elles ne sont pas toutes suivies par chaque élève.  
    Quel modèle relationnel pourrait être mis en place ?
    ??? done "Attendu"
        |Eleve||Classe||Discipline|
        |:-|-|:-|-|:-|
        |<u>INE</u> : INTEGER||<u>numero</u> : INTEGER||<u>nom</u> : TEXT|
        |nom : TEXT||nom : TEXT||nb_heures : INTEGER|
        |prenom : TEXT||||
        |specialite1 : TEXT||||
        |specialite2 : TEXT||||
        |numerodeclasse : INTEGER||||

___
## Implémentation

Une base de données relationnelle est alors constituée d’un ensemble de **tables (relations)**.  
Un **enregistrement** dans cette table est composé de plusieurs informations distinctes appartenant à une même **entité**.  
Un **attribut** est une information élémentaire appartenant à un enregistrement. Les attributs sont les intitulés de colonne : **champs**. Chaque attribut possède un **type** (propre au SGBD).  
Le **domaine** d'un attribut donné correspond à un ensemble (fini ou infini) de valeurs admissibles.

!!! summary "Résumé :"
    |Modèle relationnel|SGBD|
    |:-|:-|
    |relations (groupe d'entités communes)|tables (tableaux)|
    |entités (objets du modèle), n-uplets|enregistrements (lignes)|
    |attributs (qualificatifs des entités)|champs (colonnes)|
    |domaine|valeurs limites|
    |attributs distinctifs d'une relation|clés primaires d'une table|

___
## Langage SQL

Ce langage permet d’effectuer des **requêtes** sur les bases de données.  
En pratique, on peut programmer des requêtes SQL en Python, PHP ... ou bien utiliser une interface graphique (phpMyAdmin très utilisée pour travailler sur les bases de données MySQL des sites web).

On interroge les bases de données avec différents opérateurs sur les relations.

- **Projection** :  renvoie une copie de la table contenant seulement les attributs mentionnés. On sélectionne des colonnes.
- **Sélection** :  renvoie une table contenant seulement les enregistrements vérifiant une condition donnée. On sélectionne des lignes.
- **Jointure** :  sert à « coller » deux tables ayant des éléments en commun. 

### Requête

La forme générale d’une requête SQL est :

```sql
SELECT attributs    	projection
FROM relations    	    produit
WHERE condition 	    sélection (optionnelle)
```

!!! example "Exemples :"
    
    === "exemple 1"
        Renvoie une table avec les noms et prénoms des élèves et le nom des disciplines qu'ils suivent :
        ```sql
        SELECT "nom", "prenom", "specialite1", "specialite2"
        FROM Eleves
        ```
                
    === "exemple 2"
        Renvoie une table avec les noms et prénoms des élèves dans la classe 2 :  
        ```sql
        SELECT "nom", "prenom"
        FROM Eleves
        WHERE "numero de classe" = 2
        ```
        Pour éviter d'éventuels doublons : 
        ```sql
        SELECT DISTINCT "nom", "prenom"
        FROM Eleves
        WHERE "numero de classe" = 2
        ```


Sélection sans projection : le symbole * remplace tous les attributs de la table mentionnée : SELECT ***

??? note "Compléments sur le renommage"
    Des relations peuvent avoir des attributs nommés de façon identique.  
    On peut les distinguer en précisant le nom de la relation avant l'attribut :  
        `Elèves.nom` <-------> `Discpline.nom`

??? note "Critères de sélection"
    !!! warning inline end "Utilisation de NULL"
        On n’écrit pas :  
        attribut = NULL ou  
        attribut <> NULL.  
        
    La clause **WHERE** est suivie d’une condition qui peut être exprimée à l’aide d’égalités `=`, d’inégalités `<, >, <>`, des opérateurs **`AND, OR, NOT, BETWEEN, IN, LIKE, IS NULL, IS NOT NULL`**.  
    **`IS (NOT) NULL`** sert à tester si un attribut prend (ou pas) la valeur **NULL**.


!!! example "Exemples :"
    === "exemple 1"
        Renvoie la liste des élèves des classes numérotées 3 à 6
        ```sql
        SELECT "nom", "prenom"
        FROM Eleves
        WHERE "numero de classe" > 2
        ```

    === "exemple 2"
        Renvoie la liste des élèves dont le nom commence par D.
        ```sql
        SELECT "nom", "prenom"
        FROM Eleves
        WHERE "nom" LIKE "D%"
        ```

**`LIKE`** 'nom' recherche le nom exact  
Underscore `_` peut remplacer un caractère unique.
`%` remplace une chaîne de caractères de longueur quelconque.


### Jointure

La syntaxe est :
```sql
SELECT attributs
FROM relation_primaire
INNER JOIN relation secondaire ON attribut_primaire = attribut_secondaire
WHERE condition
```

!!! example "Exemple :"
    Renvoie la table des élèves qui suivent la spécialité NSI avec le nom de leur classe
    ```sql
    SELECT Elèves.nom, prenom, Classe.nom
    FROM Elèves
    INNER JOIN Classe ON "numero de classe" = numero
    WHERE specialite1 = "NSI" OR specialite2 = "NSI"
    ```

### Fonctions d’agrégation

Une fonction d’agrégation prend une collection de valeurs et retourne une valeur unique.  
Les principales sont **`AVG, COUNT, MAX, MIN, SUM`**.

`AVG` (moyenne) et `SUM` (somme) s’appliquent uniquement à des données numériques.

`COUNT(∗)` renvoie le nombre de lignes dans la table  
`COUNT(A)` renvoie le nombre de lignes dans la colonne A dont le contenu n’est pas NULL.

On peut utiliser `DISTINCT` à l’intérieur de `COUNT` :  
```sql
SELECT COUNT (DISTINCT nom)
FROM relation
WHERE condition
```

!!! example "Exemple : "
    Renvoie le nombre d'élèves dont le nom commence par la lettre A.
    ```sql
    SELECT COUNT (DISTINCT nom)
    FROM Elèves
    WHERE nom LIKE "A%"
    ```

### Insertion

Cela permet de rajouter des entités à une relation.

```sql
INSERT INTO relation
VALUES (...)
``` 

!!! example "Exemple : Nouvel élève"
    ```sql
    INSERT INTO Elèves
    VALUES (150, "Sahalor", "Aubin", "SES", "S. Phy.", 4)
    ```

### Modification

 Cela permet de modifier la valeur d’un ou plusieurs attributs.

```sql
UPDATE relation
SET attribut1 = valeur1, attibut2 = valeur2, ... 
WHERE condition
```

!!! example "Exemple : Elève changeant de classe"
    ```sql
    UPDATE Elèves
    SET "numero de classe" = 2  
    WHERE nom = "Fournier"
    ```


### Suppression

Cela permet d’effacer des entités.

```sql
DELETE FROM relation
WHERE condition
```

!!! example "Exemple : Elève changeant d'établissement"
    ```sql
    DELETE FROM Elèves
    WHERE nom = "Bernard"
    ```


## Contraintes d’intégrité

Les contraintes d’intégrité sont des règles que l’on impose à la base de données afin de garantir la cohérence des données qu'elle contient :

- **Contrainte de relation** :
Chaque tuple d'une relation doit pouvoir être identifié par un attribut unique et non nul : **clé primaire**.  
Chaque relation doit en contenir une.

- **Contraintes de référence** :
Lorsqu'un attribut d'une table fait référence à une clé primaire d'une autre table, on l'appelle une **clé étrangère** : une référence entre les 2 relations est créée.  
Il en découle 3 règles :  
  1) Une clé étrangère ne peut être une valeur qui n'est pas clé primaire de la table à laquelle elle se réfère.  
  2) Un enregistrement de la table primaire ne peut être effacé s'il possède des enregistrements liés.  
  3) Une clé primaire ne peut être changée si cet enregistrement possède des liaisons avec d'autres enregistrements.


- **Contrainte de domaine** :
Chaque donnée doit avoir un type défini.  
Les types dépendent du SGBD utilisé.

??? example "Quelques exemples de type :"
    |Nom du type /mot clé|Description|
    |:-|:-|
    |SMALLINT|Entier de 16 bits signés (valeur exacte)|
    |INTEGER|Entier de 32 bits signés (valeur exacte)|
    |INT|Alias pour INTEGER|
    |BIGINT|Entier de 64 bits signés (valeur exacte)|
    |DECIMAL(t,f)|Décimal avec de t chiffres dont f chiffres après virgule (valeur exacte)|
    |REAL|Flottant de 32 bits (valeur approchée)|
    |DOUBLE PRECISION|Flottant de 32 bits (valeur approchée)|
    |CHAR(n)|Chaîne de n caractères|
    |VARCHAR(n)|Chaîne d'au plus n caractères|
    |TEXT|Chaîne de taille quelconque|
    |BOOLEAN|Type booléen parfois non supportés|
    |DATE|Date au format 'AAAA-MM-JJ'|
    |TIME|Heure au format 'hh\:mm:ss'|
    |TIMESTAMP|Un instant (date et heure) au format 'AAAA-MM-JJ hh\:mm:ss'|
    |NULL|Il existe une valeur NULL (comme None en Python)|

Une donnée peut également appartenir à un domaine spécifique de valeurs (ex : une note comprise entre 0 et 20).
