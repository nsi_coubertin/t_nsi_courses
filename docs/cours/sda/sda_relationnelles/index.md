___
Un **graphe** est une structure de données représentant des objets possédant des relations entr'eux.

![graphe_intro](images/intro.png){width=80%}

## Graphes simples (non orientés)

Les relations entre les objets sont seulement symétriques.

!!! example "Exemple avec un réseau social (*faceudbouc*)"
    !!! note inline end "lecture du graphe"  
        'a' est ami avec 'b' et donc 'b' est ami avec 'a'  
        'a' est aussi ami avec 'c' et 'e'  
        'b' est ami avec 'c'  
        'c' est ami avec 'e' et 'd'
    ![graphe1](images/graphe1.png){width=60%}

!!! info inline end "Compléments"
    Ordre d'un graphe = nombre de sommets  
    Degré d'un sommet = nombre de voisins accessibles

Le graphe possède :

  - des **sommets** (objets)  
  - des **arêtes** (relations)


## Graphes orientés

Les relations entre les objets sont asymétriques ou symétriques.

!!! example "Exemple avec un autre réseau social (*truiteur*)"
    !!! note inline end "Lecture du graphe"  
        'a' suit 'b'  
        'b' suit 'c'  
        'c' suit 'd',  
        'd' suit 'a'  
        Mais 'b' ne suit pas 'a'  
        et ''c' ne suit pas 'b'
    ![graphe2](images/graphe2.png){width=45%}

Le graphe orienté possède :

  - des **sommets** (objets) et des **successeurs** (voisins du sommet)  
  - des **arcs** (relations orientées)

!!! warning "Attion aux orientations"
    'b' est le successeur de 'a' (l'inverse est faux)


##  Graphes pondérés (orientés ou pas)

Les relations entre les objets ne sont pas équivalentes.

!!! example "Exemple avec un réseau routier"
    !!! note inline end "Lecture du graphe"    
        De Paris, la distance n'est pas la même pour Marseille et Nice.  
        De Paris à Cannes, 2 chemins sont possibles mais ne pas pas équivalents.
    ![graphe3](images/graphe3.png){width=50%}

Le graphe pondéré possède :

  - des **sommets** (objets)
  - des **poids** (sur les relations orientées ou non)


## Implémentations avec un dictionnaire

!!! info inline end "Info"
     Cette version préconisée par G. V. Rossum. *(créateur de Python)*

Un dictionnaire permet d'implémenter un graphe en Python :

  - **clé** : sommet  
  - **valeur** : tableau des sommets voisins (successeurs pour les graphes orientés), avec éventuellement le poids pour chaque relation (pour les graphes pondérés)


##  Implémentation avec un tableau de tableaux

!!! info inline end "Complément"
    La table est aussi appelé aussi **matrice d'adjacence**.

Une table peut aussi implémenter un graphe en Python (différence : elle fait apparaître toutes les relations possibles).

  - **ligne** : sommet de départ  
  - **colonne** : sommet d'arrivée (ou successeur)  
  - **valeur** : poids de la relation  

!!! example "Graphe non pondéré"
    ![graphe1](images/graphe1.png){width=60%}
    ```python
    dico = { 'a' : ['b', 'c', 'e'],
             'b' : ['a', 'c'],
             'c' : ['a', 'b', 'd', 'e'],
             'd' : ['c'],
             'e' : ['a', 'c'] }

    table = [ [ 0, 1, 1, 0, 1 ]         # a          0 : aucune relation
              [ 1, 0, 1, 0, 0 ],        # b          1 : une relation (ligne - colonne)
              [ 1, 1, 0, 1, 1 ],        # c                              d   -   c
              [ 0, 0, 1, 0, 0 ],        # d
              [ 1, 0, 1, 0, 0 ] ]       # e

    #           a  b  c  d  e
    ```

!!! example "Graphe orienté"
    ![graphe2](images/graphe2.png){width=50%}
    ```python
    dico = { 'a' : ['b'],
             'b' : ['c'],
             'c' : ['d'],
             'd' : ['a'] }

    table = [ [ 0, 1, 0, 0 ],
              [ 0, 0, 1, 0 ],
              [ 0, 0, 0, 1 ],
              [ 1, 0, 0, 0 ] ]
    ```

!!! example "Graphe pondéré, non orienté"
    ![graphe3](images/graphe3.png){width=60%}
    ```python
    dico = { 'C' : [('M', 181), ('N', 33)],
             'M' : [('P', 777), ('C', 181)],
             'N' : [('P', 932), ('C', 33)],
             'P' : [('M', 777), ('N', 932)] }

    table = [ [ 0, 181, 33, 0 ],
              [ 181, 0, 0, 777 ],
              [ 33, 0, 0, 932 ],
              [ 0, 777, 932, 0 ] ]
    ```


## Parcours dans un graphe

**Parcourir un graphe** : on part d'un sommet (sommet de départ) et on explore tous les sommets que l'on peut atteindre grâce aux relations établies. Le **chemin** s'établit grâce à la chaîne des sommets parcourus.

Un graphe est **connexe** lorsqu'on peut atteindre tous les sommets à partir d'un sommet de départ (quelque soit le sommet choisi).

Un graphe possède un **cycle** si lors d'un parcours, on repasse par un sommet déjà exploré.

!!! question "Exercice 1"
    1. Implémenter ce graphe (dico et table) :  
    ![graphe4](images/graphe4.png){width=80%}  
    2. Proposer une implémentation avec une classe d'objet.


### Parcours en largeur

On explore en priorité tous les voisins du sommet de départ, puis tous les voisins des voisins, et ainsi de suite.

!!!summary "algorithme :"
    fonction parcours largeur à partir d'un sommet de départ :

    - créer un parcours vide
    - créer une **file** (taille = nb de sommets)
    - **enfiler** le sommet de départ
    - **tant que** la file n'est pas vide
        - **défiler** un sommet de la file
        - ajouter ce sommet dans le parcours
        - **pour** chaque voisin du sommet
            - si le voisin n'est ni dans la file, ni dans le parcours
                - **enfiler** ce voisin dans la file
    - renvoyer le parcours


!!! example "Exemple sur le graphe précédent" 
    |Exploration|Parcours|
    |:--|--:|
    |En partant du sommet **A**|'**A**'|
    |On explore ses voisins : **B** et **C** |'A**BC**'|
    |On explore les voisins des voisins précédents : || 
    | - pour B : **D** et **E** | 'ABC**DE**'|
    | - pour C : **F** | 'ABCDE**F**'|
    |On explore les voisins des voisins précédents : ||
    | - pour D : F, **G**, **H**   *(mais F déjà vu dans le parcours)* | 'ABCDEF**GH**'|
    | - pour E : F *(mais F déjà vu dans le parcours)* | 'ABCDEFGH'|
    | - pour F : None | 'ABCDEFGH'|
    |On explore les voisins des voisins précédents : ||
    | - pour G : H *(mais H déjà vu dans le parcours)*  | 'ABCDEFGH'|
    | - pour H : None | 'ABCDEFGH'|
    |Plus de sommet à explorer : fin| '**ABCDEFGH**'|  
    *NB : On pouvait s'arrêter après l'exploration des voisins de D car le parcours contenait déjà tous les sommets.*

Autre exemple : En partant du sommet B, on obtient BDEFGH.

!!! warning "Lors d'un parcours en largeur, on atteint pas forcément tous les sommets."
    Notamment pour les graphes orientés.

!!! question "Exercice 2"
    Implémenter le parcours en largeur et retrouver les résultats précédents.  
    *On préférera une implémentation du graphe avec un dictionnaire.*


### Parcours en profondeur

On explore en priorité les voisins du premier voisin, puis les voisins du premier voisin de ceux-ci, et ainsi de suite.  
*Le choix du premier voisin est totalement arbitraire : ordre alphabétique, par exemple.*

!!!summary "algorithme :"
    fonction parcours profondeur à partir d'un sommet de départ :

    - créer un parcours vide
    - créer une **pile** (taille = nb de sommets)
    - **empiler** le sommet de départ
    - **tant que** la **file** n'est pas vide
        - **défiler** le premier sommet de la pile
        - ajouter ce sommet dans le parcours
        - **pour** chaque voisin du sommet
            - si le voisin n'est ni dans la pile, ni dans le parcours
                - **empiler** ce voisin dans la file
    - renvoyer le parcours


!!! example "Sur le même graphe"
    |Exploration|Parcours|
    |:--|--:|
    |En partant du sommet **A**|'**A**'|
    |On explore un de ses voisins : **B** ou **C** ||
    | - avec B | 'A**B**'|
    |On explore un des voisins de B : **D** ou **E** ||
    | - avec D : | 'AB**D**'|
    |On explore un des voisins de D : **F** ou **G** ou **H** ||
    | - avec F : | 'ABD**F**'|
    |On explore un des voisins de F : None ||
    |On poursuit alors avec un des voisins du sommet précédent :  D||
    | - pour G : | 'ABDF**G**'|
    |On explore un des voisins de G : **H** ||
    | - pour H : | 'ABDFG**H**'|
    |On explore un des voisins de H : None ||
    |On poursuit alors avec un des voisins du sommet précédent :  D||
    | - pour H : *H déjà vu dans le parcours* ||
    |On poursuit alors avec un des voisins du sommet précédent :  B||
    | - pour E : | 'ABDFGH**E**'|
    |On explore un des voisins de E : **F** ||
    | - pour F : *F déjà vu dans le parcours*  ||
    |On poursuit alors avec un des voisins du sommet précédent :  A||
    | - pour C : | 'ABDFGHE**C**'|
    |On explore un des voisins de C : **F** ||
    | - pour F : *F déjà vu dans le parcours*  ||
    |On poursuit alors avec un des voisins du sommet précédent :  A||
    |Plus de sommet à explorer : fin | '**ABDFGHEC**'|

Autre Exemple : En partant du sommet B, on obtient BDFGHE.

!!! warning "Lors d'un parcours en profondeur, on atteint pas forcément tous les sommets."
    Notamment pour les graphes orientés.

!!! question "Exercice 3"
    Implémenter le parcours en profondeur et retrouver les résultats précédents.  
    *On préférera une implémentation du graphe avec un dictionnaire.*