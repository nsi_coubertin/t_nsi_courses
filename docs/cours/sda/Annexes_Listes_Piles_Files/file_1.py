# -*- coding: utf-8 -*-
'''
:Titre : Implémentation d'une file, version 1 : POO
:Auteur : L. Conoir
:Date : 10/2020
'''

class File(object):        # en anglais : queue
    
    def __init__(self, capacite):
        'Constructeur de la classe'
        self.__elements = []
        self.__capacite = capacite
    
    
    def Est_Vide(self):
        'Méthode publique, précise si la file est vide'
        return len(self.__elements) == 0
            
    
    def Est_Pleine(self):
        'Méthode publique, précise si la file est pleine'
        return len(self.__elements) == self.__capacite
    
    
    def Enfiler(self, element):
        'Méthode publique, enfile un nouvel element (à la fin).'
        if not self.Est_Pleine():
            self.__elements.append(element)
        else:
            print('MI : La file est pleine.')    # Queue Overflow
    
    def Defiler(self, file):
        'Méthode publique, défile le premier et le renvoie.'
        if not self.Est_Vide :
            return self.__elements.pop(0)
        else:
            print('MI : La file est vide.')
    
    
    def Nombre_Elements(self):
        "Méthode publique, renvoie le nombre d'éléments dans la file."
        return len(self.__elements)
    
    
    def Capacite(self):
        'Méthode publique, renvoie la capacité de la file.'
        return self.__capacite


#################################################################################
class File_bis(object):
        
    def __init__(self, capacite):
        self.__premier = None
        self.__suite = []
        self.__capacite = capacite
        self.__nb_elements = 0
        
    def Est_Vide(self):
        return self.__nb_elements == 0
    
    def Est_Pleine(self):
        return self.__nb_elements == self.__capacite
        
    def Enfiler(self, element):
        if not self.Est_Pleine() :
            self.__suite.append(element)
            self.__nb_elements += 1
        else :
            print('La file est pleine.')
            
    def Defiler(self):
        if not self.Est_Vide() :
            element = self.__premier
            self.__nb_elements -= 1
            if self.__suite != [] :
                self.__premier = self.__suite.pop(0)
            else :
                self.__premier = None
            return element
        else :
            print('La file est vide.')
        
    def Nombre_Element(self):
        return self.__nb_element
    
    def Capacite(self):
        return self.__capcite
