# -*- coding: utf-8 -*-
'''
:Titre : Implémentation d'une pile, version 2 : fonctionnel
:Auteur : L. Conoir
:Date : 10/2020
'''

def Creer_Pile_Vide(capacite):
    '''La fonction renvoie un dictionnaire comportant :
     une clé 'elements' ayant pour valeur une liste vide
     une clé 'nb_elements' ayant pour valeur 0
     une clé 'capacite' ayant pour valeur la valeur de la variable capacite
:example :
>>> Creer_Pile_Vide(10)
{'elements': [], 'nb_elements': 0, 'capacite': 10}
'''
    return {'elements' : [],
            'nb_elements' : 0,
            'capacite' : capacite}           


def Est_Vide(pile):
    '''Prédicat signalant si la pile est vide.
:example :
>>> pile_essai = {'elements': [], 'nb_elements': 0, 'capacite': 10}
>>> Est_Vide(pile_essai)
True
>>> pile_essai = {'elements': [], 'nb_elements': 1, 'capacite': 10}
>>> Est_Vide(pile_essai)
False
'''
    return pile['nb_elements'] == 0


def Est_Pleine(pile):
    '''Prédicat signalant si la pile est pleine.
:example :
>>> pile_essai = {'elements': [], 'nb_elements': 10, 'capacite': 10}
>>> Est_Pleine(pile_essai)
True
>>> pile_essai = {'elements': [], 'nb_elements': 8, 'capacite': 10}
>>> Est_Pleine(pile_essai)
False
'''
    return pile['nb_elements'] == pile['capacite']


def Empiler(pile, element):
    '''La fonction ajoute un element au sommet de la pile.
:example :
>>> pile_essai = {'elements': [5, 4, 8], 'nb_elements': 3, 'capacite': 10}
>>> Empiler(pile_essai, 6)
>>> pile_essai
{'elements': [6, 5, 4, 8], 'nb_elements': 4, 'capacite': 10}
'''
    if not Est_Pleine(pile) :
        pile['elements'] = [element] + pile['elements']
        pile['nb_elements'] += 1
    else :
        print('MI : La pile est pleine.')


def Depiler(pile):
    '''La fonction dépile le sommet, si il existe, et le renvoie.
:example :
>>> pile_essai = {'elements': [5, 4, 8], 'nb_elements': 3, 'capacite': 10}
>>> Depiler(pile_essai)
5
>>> Depiler(pile_essai)
4
>>> pile_essai
{'elements': [8], 'nb_elements': 1, 'capacite': 10}
'''
    if not Est_Vide(pile) :
        pile['nb_elements'] -= 1
        return pile['elements'].pop(0)
    else :
        print('MI : La pile est vide.')


def Nombre_Element(pile):
    '''La fonction renvoie le nombre d'éléments dans la pile.'''
    return pile['nb_elements']


def Capacite(pile):
    '''La fonction renvoie la capacité de la pile.
(nombre maximal d'éléments que peut contenir la pile)'''
    return pile['capacite']




########################################################################
def Creer_Pile_Vide_2(capacite):
    return [capacite]


def Est_Vide_2(pile):
    return len(pile) == 1


def Est_Pleine_2(pile):
    return len(pile) - 1 == pile[-1]


def Empiler_2(pile, element):
    if not Est_Pleine(pile) :
        pile = [element] + pile
    else :
        print('MI : La pile est pleine.')
    
def Depiler_2(pile):
    if not Est_Vide(pile) :
        return pile.pop(0)
    else :
        print('MI : La pile est vide.')
        
def Nombre_Element_2(pile):
    return len(pile) - 1

def Capacite_2(pile):
    return pile[-1]


###############################################################################
if __name__ == '__main__':
    import doctest
    doctest.testmod()