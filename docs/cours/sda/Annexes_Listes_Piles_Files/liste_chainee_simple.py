# -*- coding: utf-8 -*-
'''
:Titre : Implémentation simple de la liste chaînée
:Auteur : L. Conoir
:Date : 10/2020
'''

class maillon(object):
    
    def __init__(self, element):
        self.__element = element
        self.__suivant = None         # pas de maillon rattaché derrière
        
    def Set_queue(self, suite):
        self.__suivant = suite


class liste_chainee(object):
    
    def __init__(self, tete = None):
        self.__tete = tete            # liste vide si tete = None
        

# exemple d'utilisation

maillon1 = maillon('Lundi')
maillon2 = maillon('Mardi')
maillon3 = maillon('Mercredi')
maillon4 = maillon('Jeudi')
maillon5 = maillon('Vendredi')
maillon6 = maillon('Samedi')
maillon7 = maillon('Dimanche')

maillon1.Set_queue(maillon2)
maillon2.Set_queue(maillon3)
maillon3.Set_queue(maillon4)
maillon4.Set_queue(maillon5)
maillon5.Set_queue(maillon6)
maillon6.Set_queue(maillon7)

liste = liste_chainee(maillon1)