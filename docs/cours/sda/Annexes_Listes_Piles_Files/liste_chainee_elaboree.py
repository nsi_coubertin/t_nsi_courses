# -*- coding: utf-8 -*-
'''
:Titre : Implémentation élaborée de la liste chaînée
:Auteur : L. Conoir
:Date : 10/2020
'''

class maillon(object):
    
    def __init__(self, element):
        self.__element = element
        self.__suivant = None         # pas de maillon rattaché derrière
        
    def Set_queue(self, suite):
        self.__suivant = suite


class liste_chainee(object):
    
    def __init__(self, capacite):
        self.__tete = None            # liste vide si tete = None
        self.__capacite = capacite
        self.__nombre_elements = 0
        self.__est_vide = True
        self.__est_pleine = False

    def Inserer_Element(self, element, place):
        if not self__est_pleine :
            self.__nombre_elements += 1
            self.__est_vide = False
            if self.__nombre_elements == self.__capacite :
                self.__est_pleine = True
        pass
    
    def Recuperer_Element(self, place):
        pass
    
    def Modifier_Element(self, place):
        pass
    
    def Supprimer_Element(self, element):
        if self.Rechercher_Element(element):
            self.__nombre_element -= 1
            self.__est_pleine = False
            if self.__nombre_elements == 0 :
                self.__est_vide = True
        pass
    
    def Rechercher_Element(self, element):
        pass
    
# exemple d'utilisation

maillon1 = maillon('Lundi')
maillon2 = maillon('Mardi')
maillon3 = maillon('Mercredi')
maillon4 = maillon('Jeudi')
maillon5 = maillon('Vendredi')
maillon6 = maillon('Samedi')
maillon7 = maillon('Dimanche')

liste = liste_chainee(7)
liste.Inserer_Element(maillon1, 0)
liste.Inserer_Element(maillon2, 1)
liste.Inserer_Element(maillon3, 2)
liste.Inserer_Element(maillon4, 3)
liste.Inserer_Element(maillon5, 4)
liste.Inserer_Element(maillon6, 5)
liste.Inserer_Element(maillon7, 6)