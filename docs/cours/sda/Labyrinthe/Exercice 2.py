# -*- coding: utf-8 -*-
'''
:Titre : Exerices de réflexion
:Auteur : 
:Date : 10/2020

Exercice 2
'''
# Implémentation du labyrinthe (exercice de réflexion)
from plateau_jeu_correction import Grille

labyrinthe = Grille(8, 8)
labyrinthe.ConstruireBordure()

cases = labyrinthe.GetCases()

cases[1][1].ConstruireMur('N')
cases[1][3].ConstruireMur('N')
cases[1][4].ConstruireMur('N')
cases[1][5].ConstruireMur('N')
cases[2][0].ConstruireMur('N')
cases[2][1].ConstruireMur('N')
cases[2][2].ConstruireMur('N')
cases[2][4].ConstruireMur('N')
cases[2][5].ConstruireMur('N')
cases[2][6].ConstruireMur('N')
cases[3][0].ConstruireMur('N')
cases[3][1].ConstruireMur('N')
cases[3][4].ConstruireMur('N')
cases[3][6].ConstruireMur('N')
cases[3][7].ConstruireMur('N')
cases[4][6].ConstruireMur('N')
cases[4][7].ConstruireMur('N')
cases[5][1].ConstruireMur('N')
cases[5][5].ConstruireMur('N')
cases[6][2].ConstruireMur('N')
cases[6][3].ConstruireMur('N')
cases[7][3].ConstruireMur('N')
cases[7][4].ConstruireMur('N')
cases[7][6].ConstruireMur('N')

cases[0][1].ConstruireMur('S')
cases[0][3].ConstruireMur('S')
cases[0][4].ConstruireMur('S')
cases[0][5].ConstruireMur('S')
cases[1][0].ConstruireMur('S')
cases[1][1].ConstruireMur('S')
cases[1][2].ConstruireMur('S')
cases[1][4].ConstruireMur('S')
cases[1][5].ConstruireMur('S')
cases[1][6].ConstruireMur('S')
cases[2][0].ConstruireMur('S')
cases[2][1].ConstruireMur('S')
cases[2][4].ConstruireMur('S')
cases[2][6].ConstruireMur('S')
cases[2][7].ConstruireMur('S')
cases[3][6].ConstruireMur('S')
cases[3][7].ConstruireMur('S')
cases[4][1].ConstruireMur('S')
cases[4][5].ConstruireMur('S')
cases[5][2].ConstruireMur('S')
cases[5][3].ConstruireMur('S')
cases[6][3].ConstruireMur('S')
cases[6][4].ConstruireMur('S')
cases[6][6].ConstruireMur('S')

cases[0][3].ConstruireMur('O')
cases[0][7].ConstruireMur('O')
cases[1][4].ConstruireMur('O')
cases[2][3].ConstruireMur('O')
cases[3][2].ConstruireMur('O')
cases[3][3].ConstruireMur('O')
cases[3][4].ConstruireMur('O')
cases[3][5].ConstruireMur('O')
cases[4][1].ConstruireMur('O')
cases[4][3].ConstruireMur('O')
cases[4][4].ConstruireMur('O')
cases[4][5].ConstruireMur('O')
cases[5][1].ConstruireMur('O')
cases[5][3].ConstruireMur('O')
cases[5][4].ConstruireMur('O')
cases[5][6].ConstruireMur('O')
cases[5][7].ConstruireMur('O')
cases[6][1].ConstruireMur('O')
cases[6][2].ConstruireMur('O')
cases[6][5].ConstruireMur('O')
cases[6][6].ConstruireMur('O')
cases[6][7].ConstruireMur('O')
cases[7][1].ConstruireMur('O')

cases[0][2].ConstruireMur('E')
cases[0][6].ConstruireMur('E')
cases[1][3].ConstruireMur('E')
cases[2][2].ConstruireMur('E')
cases[3][1].ConstruireMur('E')
cases[3][2].ConstruireMur('E')
cases[3][3].ConstruireMur('E')
cases[3][4].ConstruireMur('E')
cases[4][0].ConstruireMur('E')
cases[4][2].ConstruireMur('E')
cases[4][3].ConstruireMur('E')
cases[4][4].ConstruireMur('E')
cases[5][0].ConstruireMur('E')
cases[5][2].ConstruireMur('E')
cases[5][3].ConstruireMur('E')
cases[5][5].ConstruireMur('E')
cases[5][6].ConstruireMur('E')
cases[6][0].ConstruireMur('E')
cases[6][1].ConstruireMur('E')
cases[6][4].ConstruireMur('E')
cases[6][5].ConstruireMur('E')
cases[6][6].ConstruireMur('E')
cases[7][0].ConstruireMur('E')

labyrinthe.SetCases(cases)

print(labyrinthe)

def cases_possibles(case, case_precedente = None):
    possibles = []
    murs = case.GetMurs()
    x, y = case.GetPosition()
    
    if 'N' not in murs and y > 0 :
        case_possible = cases[y - 1][x]
        if case_possible != case_precedente:
            possibles.append(case_possible)
            
    if 'S' not in murs and y < 8 :
        case_possible = cases[y + 1][x]
        if case_possible != case_precedente:
            possibles.append(case_possible)
        
    if 'O' not in murs and x > 0 :
        case_possible = cases[y][x - 1]
        if case_possible != case_precedente:
            possibles.append(case_possible)
        
    if 'E' not in murs and x < 8 :
        case_possible = cases[y][x + 1]
        if case_possible != case_precedente:
            possibles.append(case_possible)
        
    return possibles
    


def parcourt():  # a vous de jouer
    pass




