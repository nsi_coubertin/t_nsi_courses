# -*- coding: utf-8 -*-
'''
:Titre : Les graphes _ Exercices corrigés
:Auteur : L. Conoir
:Date : 11/2020
'''

###############################################################
# Exercice 1 : Implémentation des représentations des graphes #
###############################################################

# Implémentation du graphe 1
dico_graphe1 = {'A' : ['B', 'D'],
                'B' : ['A', 'C', 'E'],
                'C' : ['B', 'D'],
                'D' : ['A', 'C', 'E'],
                'E' : ['B', 'D']}

liste_graphe1 = [[0,1,0,1,0],
                 [1,0,1,0,1],
                 [0,1,0,1,0],
                 [1,0,1,0,1],
                 [0,1,0,1,0]]

# Implémentation du graphe 2
dico_graphe2 = {'A' : ['R'],
                'B' : ['R'],
                'C' : ['R'],
                'D' : ['R', 'F', 'G'],
                'E' : ['R'],
                'F' : ['D'],
                'G' : ['D'],
                'R' : ['A', 'B', 'C', 'D', 'E']}

liste_graphe2 = [[0,0,0,0,0,0,0,1],
                 [0,0,0,0,0,0,0,1],
                 [0,0,0,0,0,0,0,1],
                 [0,0,0,0,0,1,1,1],
                 [0,0,0,0,0,0,0,1],
                 [0,0,0,1,0,0,0,0],
                 [0,0,0,1,0,0,0,0],
                 [1,1,1,1,1,0,0,0]]


##################################################################################
# Exercice 2 : Implémentation des fonctions précisant l'existence d'une relation #
##################################################################################
def relation_dico(dico, S1, S2):
    '''Prédicat donnant l'existance d'une relation du somet 'S1' vers le sommet 'S2' dans le graphe défini par 'dico'.

:param: dico, type dict, dictionnaire d'adjacence du graphe
:param: S1,S2, type str, 2 sommets du graphe
:return: type boolean, existence de la relation S1 --> S2
:CU: les paramètres sont du type défini

:examples:
>>> relation_dico(dico_graphe1, 'A', 'B')
True
>>> relation_dico(dico_graphe2, 'A', 'B')
False
'''
    assert isinstance(dico, dict), "Le dictionnaire n'est pas correct."      # vérification des types des paramètres
    assert isinstance(S1, str) and isinstance(S2, str), "Un des sommets n'est pas de type str."
    
    liste = dico.get(S1)          # récupération éventuelle de la liste des sommets voisins de S1, si il est référencé comme clé dans dico
    if liste != None :
        return S2 in liste        # évaluation de la présence de S2 dans la liste
    

def relation_table(liste, S1, S2, dico_sommets = {'A' : 0, 'B' : 1, 'C' : 2, 'D' : 3, 'E' : 4}):
    '''Prédicat donnant l'existance d'une relation du somet 'S1' vers le sommet 'S2' dans le graphe défini par 'table'.

:param: table, type list, liste d'adjacence du graphe
:param: S1,S2, type str, 2 sommets du graphe
:param: dico_sommets, type dict, références des indices utilisés dans la liste par rapport aux noms des sommets
:return: type boolean, existence de la relation S1 --> S2
:CU: les paramètres sont du type défini

:examples:
>>> relation_table(liste_graphe1, 'A', 'B')
True
>>> relation_table(liste_graphe2, 'A', 'B', {'A' : 0, 'B' : 1, 'C' : 2, 'D' : 3, 'E' : 4, 'F' : 5, 'G' : 6, 'R' : 7})
False
'''
    assert isinstance(liste, list), "La liste n'est pas correcte."      # vérification des types des paramètres
    assert isinstance(S1, str) and isinstance(S2, str), "Un des sommets n'est pas de type str."
    
    indice1 = dico_sommets.get(S1)          # récupérations éventuelles de l'indice des sommets si ils sont référencés comme clé dans dico_sommets
    indice2 = dico_sommets.get(S2)
    
    if indice1 != None and indice2 != None :
        return liste[indice1][indice2] != 0     # évaluation de la présence d'une valeur de relation


# en une seule fonction
def relation_graphe(graphe, S1, S2, dico_sommets = {'A' : 0, 'B' : 1, 'C' : 2, 'D' : 3, 'E' : 4, 'F' : 5, 'G' : 6, 'R' : 7}):
    '''Prédicat donnant l'existance d'une relation du somet 'S1' vers le sommet 'S2' dans le 'graphe'.
Le graphe peut être implémenté sous forme de liste ou dictionnaire d'adjacence.'''
    if isinstance(graphe, dict) :
        
        liste = graphe.get(S1)
        if liste != None :
            return S2 in dico[S1]
    
    elif isinstance(graphe, list) :
        indice1 = dico_sommets.get(S1)
        indice2 = dico_sommets.get(S2)
        if indice1 != None and indice2 != None :
            return graphe[indice1][indice2] != 0


#########################################################################################################
# Exercice 3 : Implémentation des fonctions de conversion entre le dictionnaire et la table d'un graphe #
#########################################################################################################
def conversion_dico_table(dico, liste_sommets = ['A', 'B', 'C', 'D', 'E']):
    '''La fonction revoie une liste équivalente au dictionnaire d'adjacence 'dico' pour un graphe
dont les sommets sont dans la liste.

:param: dico, type dict, dictionnaire d'adjacence du graphe
:param: liste_sommets, type list, liste des sommets du graphe
:return: type list, liste d'adjacence du graphe
:CU: les paramètres sont du type défini, les sommets référencés dans 'dico' sont dans 'liste_sommets'

:Examples:
>>> conversion_dico_table(dico_graphe1)
[[0, 1, 0, 1, 0], [1, 0, 1, 0, 1], [0, 1, 0, 1, 0], [1, 0, 1, 0, 1], [0, 1, 0, 1, 0]]
>>> conversion_dico_table(dico_graphe2, ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'R'])
[[0, 0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 0, 1, 1, 1], [0, 0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 1, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0, 0, 0], [1, 1, 1, 1, 1, 0, 0, 0]]
'''
    for sommet in dico.keys() :        # vérification que les sommets du dico sont dans la liste des sommets
        assert sommet in liste_sommets,"Le sommet " + str(sommet) + "trouvé dans le dictionnaire n'est pas dans la liste des sommets."
    
    table = []
    nb_sommet = len(liste_sommets)
    
    for ligne in range(nb_sommet) :
        
        liste = []
        sommet1 = liste_sommets[ligne]                    # récupération du nom du sommet 1
        
        for colonne in range(nb_sommet) :
        
            sommet2 = liste_sommets[colonne]              # récupération du nom du sommet 2
            
            if relation_dico(dico, sommet1, sommet2) :    # vérification d'une relation
                
                liste.append(1)                           # dans le cas d'un graphe non pondéré
            
            else :
            
                liste.append(0)
        
        table.append(liste)            # ajout de la liste 
        
    return table
        


def conversion_table_dico(liste, liste_sommets = ['A', 'B', 'C', 'D', 'E']):
    '''La fonction revoie un dictionnaire équivalente à la liste d'adjacence 'liste' pour un graphe
dont les sommets sont dans la liste.

:param: liste, type list, liste d'adjacence du graphe
:param: liste_sommets, type list, liste des sommets du graphe
:return: type dict, dictionnaire d'adjacence du graphe
:CU: les paramètres sont du type défini

:Examples:
>>> conversion_table_dico(liste_graphe1)
{'A': ['B', 'D'], 'B': ['A', 'C', 'E'], 'C': ['B', 'D'], 'D': ['A', 'C', 'E'], 'E': ['B', 'D']}
>>> conversion_table_dico(liste_graphe2, ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'R'])
{'A': ['R'], 'B': ['R'], 'C': ['R'], 'D': ['F', 'G', 'R'], 'E': ['R'], 'F': ['D'], 'G': ['D'], 'R': ['A', 'B', 'C', 'D', 'E']}
'''
    dico = {}
    nb_sommet = len(liste_sommets)
        
    for ligne in range(nb_sommet) :                 # lecture de chaque ligne de la table : liste des relations avec S1
            
        sommet1 = liste_sommets[ligne]              # référence de l'indice de ligne avec le sommet 1
        liste_voisins = []
            
        for colonne in range(nb_sommet) :           # lecture de chaque colonne dans la ligne en cours : S2
            
            if liste[ligne][colonne] != 0 :         # vérification de la relation
                
                sommet2 = liste_sommets[colonne]    # référence de l'indice de colonne avec le sommet 2
                liste_voisins.append(sommet2)
            
        dico[sommet1] = liste_voisins
        
    return dico



################################################################
# Vérification des tests unitaires dans chacunes des fonctions #
################################################################
if __name__ == '__main__':
    import doctest
    doctest.testmod()