# -*- coding: utf-8 -*-
'''
:Titre : Implémentation d'un arbre, version 3 : dictionnaire
:Auteur : L. Conoir
:Date : 10/2020
'''


def creation_arbre(racine):
    '''La fonction crée un arbre composé de sa racine sous forme de dictionnaire :
    {'racine': racine, racine: [sous-arbre gauche, sous-arbre droit], noeud2 ...}
    
:exemple:
>>> creation_arbre(5)
{'racine': 5, 5: [None, None]}
'''
    return {'racine': racine, racine: [None, None]}


def ajout_SAG(arbre, noeud, s_arbre):
    '''La fonction ajoute le sous-arbre s_arbre dans l'arbre au niveau du sous-arbre
de gauche du noeud, si la place est libre.
:exemple:
>>> arbre_1 = {'racine': 5, 5: [None, 2], 2: [None, None]}
>>> arbre_2 = {'racine': 3, 3: [1, None], 1: [None, None]}
>>> ajout_SAG(arbre_1, 5, arbre_2)
>>> arbre_1
{'racine': 5, 5: [3, 2], 2: [None, None], 3: [1, None], 1: [None, None]}
'''
    if noeud in arbre.keys() and arbre[noeud][0] == None:
            arbre[noeud][0] = s_arbre['racine']
            for cle, valeur in s_arbre.items():
                if cle != 'racine':
                    arbre[cle] = valeur


def ajout_SAD(arbre, noeud, s_arbre):
    '''La fonction ajoute le sous-arbre s_arbre dans l'arbre au niveau du sous-arbre
de gauche du noeud, si la place est libre.
:exemple:
>>> arbre_1 = {'racine': 5, 5: [2, None], 2: [None, None]}
>>> arbre_2 = {'racine': 3, 3: [1, None], 1: [None, None]}
>>> ajout_SAD(arbre_1, 2, arbre_2)
>>> arbre_1
{'racine': 5, 5: [2, None], 2: [None, 3], 3: [1, None], 1: [None, None]}
'''
    if noeud in arbre.keys() and arbre[noeud][1] == None:
            arbre[noeud][1] = s_arbre['racine']
            for cle, valeur in s_arbre.items():
                if cle != 'racine':
                    arbre[cle] = valeur
            
   
def taille(arbre):
    '''Renvoie la taille de l'arbre
:exemple:
>>> arbre_1 = {'racine': 5, 5: [2, None], 2: [None, 3], 3: [1, None], 1: [None, None]}
>>> taille(arbre_1)
4
'''
    return len(arbre) - 1


def hauteur(arbre, noeud = 'racine'):
    '''Renvoie la hauteur de l'arbre
:exemple:
>>> arbre_1 = {'racine': 5, 5: [2, None], 2: [None, 3], 3: [1, None], 1: [None, None]}
>>> hauteur(arbre_1)
4
'''
    if arbre == None or noeud == None:
        return 0
    if noeud == 'racine' :
        noeud = arbre[noeud]
    return 1 + max(hauteur(arbre, arbre[noeud][0]), hauteur(arbre, arbre[noeud][1]))


def parcours_largeur(arbre):
    '''Renvoie le parcours en largeur
:exemple:
>>> parcours_largeur(arbre2)
[5, 2, 6, 1, 3, 9]
'''
    from file_1 import File
    
    file = File(taille(arbre))
    file.Enfiler(arbre['racine'])
    parcours = []
    
    while not file.Est_Vide():
        noeud = file.Defiler()
        parcours.append(noeud)
        
        SAG, SAD = arbre[noeud][0], arbre[noeud][1]
    
        if SAG != None:
            file.Enfiler(SAG)
        
        if SAD != None:
            file.Enfiler(SAD)
    
    return parcours


def parcours_prefixe(arbre, noeud = 'racine'):
    '''Renvoie le parcours en profondeur dans l'ordre préfixe
:exemple:
>>> parcours_prefixe(arbre2)
[5, 2, 1, 6, 3, 9]
'''
    if noeud == None:
        return []
    else :
        if noeud == 'racine':
            noeud = arbre[noeud]
        return [noeud] + parcours_prefixe(arbre, arbre[noeud][0]) + parcours_prefixe(arbre, arbre[noeud][1])


def parcours_infixe(arbre, noeud = 'racine'):
    '''Renvoie le parcours en profondeur dans l'ordre infixe
:exemple:
>>> parcours_infixe(arbre2)
[1, 2, 5, 3, 6, 9]
'''
    if noeud == None:
        return []
    else :
        if noeud == 'racine':
            noeud = arbre[noeud]
        return parcours_infixe(arbre, arbre[noeud][0]) + [noeud] + parcours_infixe(arbre, arbre[noeud][1])


def parcours_suffixe(arbre, noeud = 'racine'):
    '''Renvoie le parcours en profondeur dans l'ordre suffixe
:exemple:
>>> parcours_suffixe(arbre2)
[1, 2, 3, 9, 6, 5]
'''
    if noeud == None:
        return []
    else :
        if noeud == 'racine':
            noeud = arbre[noeud]
        return parcours_suffixe(arbre, arbre[noeud][0]) + parcours_suffixe(arbre, arbre[noeud][1]) + [noeud]
    
    

#######################################################
if __name__ == '__main__':
    
    arbre2 = creation_arbre(5)

    ajout_SAG(arbre2, 5, creation_arbre(2))
    ajout_SAG(arbre2, 2, creation_arbre(1))

    ajout_SAD(arbre2, 5, creation_arbre(6))
    ajout_SAG(arbre2, 6, creation_arbre(3))
    ajout_SAD(arbre2, 6, creation_arbre(9))
    
    import doctest
    doctest.testmod()