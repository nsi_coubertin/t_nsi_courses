___
Les données sont traitées séquentiellement : les unes après les autres.

## Les listes

Une liste est une suite **finie** d'éléments. Chaque élément a une place définie.  
Une liste peut être vide (aucun élément).

On la définit généralement avec un élément de **tête** (noté aussi *car*) et le reste des éléments, appelé **queue** de la liste (noté aussi *cdr*) : image de la chenille avec sa tête et ses maillons.

!!! example "Exemple : les jours de la semaine"
    ![liste2](images/liste2.png){width=90%}

Les éléments sont enchaînés les uns derrière les autres : on parle ici de **liste chaînée **à simple chaînage (un lien entre le maillon et le maillon suivant).  
*(double chaînage : le maillon est lié au maillon précédent et au maillon précédent)*

Les indices ne sont pas forcément nécessaires mais facilitent grandement l'utilisation.

!!! note "Exemples de routines associées :"

    - `Creer_Liste_Vide() → liste`
    - `Inserer_Element(liste, element, place)`
    - `Recuperer_Element(liste, place) → element` *(si il existe)*
    - `Modifier_Element(liste, element)`
    - `Supprimer_Element(liste, element)`
    - `Rechercher_Element(liste, element) → bool`
    - `Est_Vide(liste) → bool`
    - `Est_Pleine(liste) → bool`
    - `Nombre_Element(liste) → int`
    - `Capacite(liste) → int`
    - ...

Il existe de multiples implémentations de listes, mais le principe reste toujours plus ou moins le même.

Cette structure est très courante et elle est à la base de nombreuses autres.

En **Python**, **le type** ```list``` n'est pas une liste, même si on retrouve les  mêmes routines : c'est un tableau dynamique (éléments non chaînés, emplacement mémoire différent et taille extensible).

!!! question "Exercice"
    Réaliser l'implémentation d'une liste en Python.


## Les piles

Une **pile** est une suite finie d'**éléments empilés**.  
Le dernier élément ajouté est le seul accessible : principe **LIFO** (Last In First Out)

!!! example "Exemple de fonctionnement : pile à 7 éléments"
    ![pile2](images/pile2.png){width=90%}

Si on ajoute un élément, on l'**empile** sur les autres.  
On ne peut retirer que le sommet de la pile à chaque fois : on le **dépile**.

!!! example "Exemples de routines associées :"

    - `Creer_Pile_Vide() → pile`
    - `Empiler(pile, élément)`
    - `Dépiler(pile) → sommet`
    - `Est_Vide(pile) → bool`
    - `Est_Pleine(pile) → bool`
    - `Nombre_Element(pile) → int`
    - `Capacite(pile) → int`
    - ...

!!! note "Applications :"

    - enregistrement des actions réalisées pour pouvoir revenir en arrière ou en avant (CTRL + Z, CTRL + Y, nécessite deux piles)
    - répéter la dernière action
    - stocker des valeurs dans des appels récursifs
    ...

!!! question "Exercice"
    Réaliser l'implémentation d'une pile en Python.


## Les files

Une **file** est une suite finie d'**éléments enfilés**.  
Le premier élément est le seul accessible : principe **FIFO** (First In First Out)

!!! example "Exemple de fonctionnement : file à 5 éléments"
    ![file2](images/file2.png){width=90%}

Si on ajoute un élément, on l'**enfile** derrière les autres (personne dépasse).  
On ne peut retirer que le premier de la file à chaque fois : on le **défile**.

!!! example "Exemples de routines associées :"

    - `Créer_File_Vide() → file`
    - `Enfiler(file, élément)`
    - `Défiler(file) → premier`
    - `Est_Vide(file) → bool`
    - `Est_Pleine(file) → bool`
    - ...

!!! note "Applications :"

    - file d'impression
    - série d'instructions (paradigme impératif, langage machine)
    - tâche d'exécutions
    - ...

!!! question "Exercice"
    Réaliser l'implémentation d'une file en Python.
