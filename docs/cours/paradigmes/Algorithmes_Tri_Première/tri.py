# -*- coding: utf-8 -*-
'''
:Titre : Fonctions de tri
:Auteur : L. Conoir
:Date : /2020
'''


# tris comparés
nombre_comparaison = 0
nombre_permutation = 0


########################################################################################################
# Fonctions outils
########################################################################################################
def cree_liste_melangee(nombre_item) :
    '''La fonction renvoie une liste de nombre_item nombres entiers rangés aléatoirement.
:param: type int(), nombre_item, nombre d'éléments dans la liste crée
:return: type list(), liste de nombres entiers
:CU: nombre_item entier positif
:bord_effect: None
'''
    from random import randint, shuffle
    
    liste = [i for i in range(nombre_item)]
    shuffle(liste)
    
    return liste


def compare(element1, element2) :
    '''La fonction précise si element1 et element2 sont rangés dans l'ordre croissant (ou alphabétique).
:param: element1, element2, type int() or float() ou str()
:return: type bool(), True si l'ordre est respecté, False si ce n'est pas le cas.
:CU: element1 et element2 sont du même type
:bord_effect: None
:examples:
>>> compare(12, 15)
True
>>> compare(12, 12)
True
>>> compare(12, 10)
False
'''
    global nombre_comparaison
    nombre_comparaison = nombre_comparaison + 1

    return element1 <= element2


def permute(liste, indice1, indice2) :
    '''La fonction permute deux items dans la liste, dont les indices sont donnés.
:param: liste, type list(), liste d'items numériques ou alphabétiques
:param: indice1, indice 2, type int(), indices des items à permuter
:return: None
:CU: None
:bord_effect: la valeur de la variable liste est modifiée
:examples:
>>> liste_test = [2, 4, 12, 10, 20]
>>> permute(liste_test, 2, 3)
>>> liste_test
[2, 4, 10, 12, 20]
'''
    global nombre_permutation
    nombre_permutation = nombre_permutation + 1
    
    liste[indice1], liste[indice2] = liste[indice2], liste[indice1]


########################################################################################################
def tri_1(liste) :
    '''La fonction trie
:param: type list(), liste : liste de nombres
:return: type list(), liste triée
:CU: None
:bord_effect: la variable liste peut avoir sa valeur modifiée
'''
    nombre_item = len(liste)
    
    for indice in range(nombre_item - 1) :
        
        indice_minimum = indice
        minimum = liste[indice]
        
        for indice_recherche in range(indice + 1, nombre_item) :
            element = liste[indice_recherche]
        
            if compare(element, minimum) :
                indice_minimum = indice_recherche
                minimum = element
                
        if indice_minimum != indice :
                permute(liste, indice, indice_minimum)
    
    return liste


########################################################################################################
def tri_2(liste) :
    '''La fonction trie
:param: type list(), liste : liste de nombres
:return: type list(), liste triée
:CU: None
:bord_effect: la variable liste peut avoir sa valeur modifiée
'''
    nombre_item = len(liste)
    
    for indice in range(1, nombre_item) :
        
        rang = indice
        
        while rang > 0 and compare(liste[rang], liste[rang - 1]) :
        
            permute(liste, rang, rang - 1)
            rang = rang - 1
    
    return liste


########################################################################################################
def tri_3(liste) :
    '''La fonction trie
:param: type list(), liste : liste de nombres
:return: type list(), liste triée
:CU: None
:bord_effect: la variable liste peut avoir sa valeur modifiée
'''
    nombre_item = len(liste)
    
    for rang in range(nombre_item - 1) :
        
        for indice in range(nombre_item - 1 - rang) :
        
            if compare(liste[indice + 1], liste[indice]) :
                
                permute(liste, indice, indice + 1)

    return liste


########################################################################################################
def tri_4(liste) :
    '''La fonction trie
:param: type list(), liste : liste de nombres
:return: type list() : liste triée
:CU: None
:bord_effect: None
'''
    global nombre_permutation
    
    nombre_item = len(liste)
    
    if nombre_item < 2 :
        return liste
    
    if nombre_item == 2 :
        if compare(liste[0], liste[1]) :
            return liste
        else :
            nombre_permutation = nombre_permutation + 1
            return [liste[1], liste[0]]
    
    liste_g = []
    liste_d =[]
    pivot = liste[0]
    
    for indice in range(1, nombre_item) :
        item = liste[indice]
        if compare(item, pivot) :
            liste_g.append(item)
        else :
            liste_d.append(item)
    
    return tri_4(liste_g) + [pivot] + tri_4(liste_d)




for tri in (tri_1, tri_2, tri_3, tri_4) : 
    liste_1 = [14, 12, 3, 21, 5]
    nombre_comparaison = 0
    nombre_permutation = 0
    print(tri, tri(liste_1))
    print('Nombre de comparaisons :', nombre_comparaison)
    print('Nombre de permutations :', nombre_permutation)
    print()
    
pause = input()
liste_3 = liste_alea(100)
for tri in (tri_1, tri_2, tri_3, tri_4) : 
    liste_2 = liste_3.copy()
    nombre_comparaison = 0
    nombre_permutation = 0
    print(tri, tri(liste_2))
    print('Nombre de comparaisons :', nombre_comparaison)
    print('Nombre de permutations :', nombre_permutation)
    print()
