# -*- coding: utf-8 -*-
'''
:Titre : Module de classe Case(), Grille()
:Auteur : 
:Date : 
'''

class Case() :
    '''Classe définissant une case à partir de sa position : x, y.

Un objet, instance de cette classe, possède plusieurs méthodes :

    ConstruireMur() : construit un mur de la case
    DetruireMur() : détruit un mur de la case
    GetContenu() : renvoie le contenu de la case
    SetContenu() : affecte le contenu de la case
    Getposition() : renvoie la position de la case
    GetMurs() : renvoie la liste des murs de la case'''
    
    
#   A faire :

#   attributs :   position
#                 est_vide (prédicat (True ou False), précise si la case est vide)
#                 contenu (vide au départ)
#                 murs (liste des murs, vide au départ)

#   méthodes publiques : définies dans le docstring
#   méthodes privées :  SetMurs (construit les 4 murs de la case)
        
        
class Grille() :
    '''Classe définissant une grille à partir de ses dimensions
           largeur : nombre de cases en largeur
           hauteur : nombre de cases en longueur

Un objet, instance de cette classe, possède plusieurs méthodes :

    ConstruireBordure() : construit les murs sur le contour de la grille
    DetruireBordure() : détruit les murs sur le contour de la grille
    AfficheGrilleVide() : affiche la grille (sans contenu) avec tous les murs
    AffichePlateau() : affiche le plateau (avec contenu et murs éventuels des cases)'''

#   A faire :

#   attributs :    largeur (largeur de la grille)
#                  hauteur (hauteur de la grille)
#                  cases (liste des cases composant la grille, construite par méthode privée)

#   méthodes publiques : définies dans le docstring
#   méthode privée :  CreationGrille (renvoie la liste des cases)







# A décommenter lorsque fini :
# if __name__ == '__main__' :
#     plateau = Grille(10,10)
#     print('Affichage de la grille :')
#     plateau.AfficheGrilleVide()
#     print('Affichage du plateau sans bordure :')
#     plateau.AffichePlateau()
#     plateau.BordureConstruire()
#     print('Affichage du plateau avec bordure :')
#     plateau.AffichePlateau()
