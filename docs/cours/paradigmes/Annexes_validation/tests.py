# -*- coding: utf-8 -*-
'''
:Titre : Tests unitaires
:Auteur : L. Conoir
:Date : 09/2020
'''


def somme(n):
    '''Renvoie la somme des n premiers nombres entiers.
:param: n, type int
:return: type int, la somme demandée
:CU: n est de type int
:bord_effect: None
:examples
>>> somme(0)
0
>>> somme(10)
55'''
    if n == 0 :
        return 0
    else :
        return n + somme(n - 1)
    
# somme(5)


def factorielle(n):
    '''Renvoie la factorielle de n.
:param: n, type int
:return: type int
:CU: n est de type int, différent de 0
:bord_effect: None
:examples
>>> factorielle(1)
1
>>> factorielle(6)
720'''
    if n == 1 :
        return 1
    else :
        return n * factorielle(n - 1)
    
# print(factorielle(5))


##################################################################################################################
if __name__ == "__main__":
    import doctest
    doctest.testmod()

