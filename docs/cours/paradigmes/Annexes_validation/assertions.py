# -*- coding: utf-8 -*-
'''
:Titre : Validation d'un problème
:Auteur : L. Conoir
:Date : 09/2020
'''

def addition(a, b):
    '''Donne le résultat de l'addition de a et b
:param: a,b, type int, nombres entiers
:return: type int
:CU: a et b sont des entiers
:examples:
>>> addition(2, 3)
5
>>> addition(0, 3)
3'''
    assert isinstance(a, int), "Opération impossible pour a"
    assert isinstance(b, int), "Opération impossible pour b"
    return a + b

print(addition(2, 3))
print(addition(2, 5))

print(addition(2, 2.3))
# print(addition('Bonjour', 3))



##################################################################################################################
if __name__ == "__main__":
    import doctest
    doctest.testmod()