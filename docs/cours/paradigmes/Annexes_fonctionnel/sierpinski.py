def sierpinski(n, l):
    import turtle
    turtle.speed(0)
    turtle.shape("turtle")
    if n == 0 :
        for cote in range(3) :
            turtle.forward(l)
            turtle.left(120)
    else :
        sierpinski(n-1, l//2)
        turtle.forward(l//2)
        sierpinski(n-1, l//2)
        turtle.backward(l//2)
        turtle.left(60)
        turtle.forward(l//2)
        turtle.right(60)
        sierpinski(n-1, l//2)
        turtle.left(60)
        turtle.backward(l//2)
        turtle.right(60)
        

import turtle
sierpinski(4,300)