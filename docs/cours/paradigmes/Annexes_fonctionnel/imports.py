# -*- coding: utf-8 -*-
'''
:Titre : Exemples d'importations
:Auteur : L. Conoir
:Date : 09/2020
'''

# import 1
# import operations
# 
# print(' 2 + 3 =', operations.addition(2, 3))
# print(ZERO)

# import 1, variante
# import operations as calcul
# 
# print(' 2 + 3 =', calcul.addition(2, 3))
# print(ZERO)
# 
# # import 2
# from operations import *
# 
# print(' 2 + 3 =', addition(2, 3))
# print(ZERO)
# 
# import 2, variante
from operations import multiplication

print(' 2 + 3 =', multiplication(2, 3))
print(ZERO)