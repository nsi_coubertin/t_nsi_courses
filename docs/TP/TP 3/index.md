___
## Préparation

On va utiliser un SGBD (système gestionnaire de bases de données) : [**DB
Browser for SQLite**](https://sqlitebrowser.org/dl/) .

*Remarque : SQLlite est très permissif, certaines requêtes sont admises
par SQLite mais pas par d'autres versions de SQL*

On va importer des tables de données à partir de fichiers au format csv.

Les tables utilisées dans ce TP sont de taille moyenne :

- env. 5000 films dans le fichier « films.csv »
- env. 5000 réalisateurs dans le fichier « realisateurs.csv »
- env. 100 000 acteurs dans le fichier « distribution.csv ».

Vous pouvez éventuellement ouvrir le fichier films.csv, dans un éditeur
de texte, pour en voir le contenu.

Lancer DB Browser for SQLite.

### Création de la base de données ![bouton](images/bouton_nouveau.png){width=20%}

Créer la base de données dans un répertoire : **TP\_BDD**

Donner ce nom à la base de données : **bdfilms**

Annuler ensuite la fenêtre suivante (éditer la définition de la table)

1. Importer le fichier « films.csv »    Fichier --> Importer --> Table depuis un fichier CSV

![fenetre](images/fenetre_config.png){width=40%}

Préciser certains éléments :

-   Champs (nom des colonnes en 1^ère^ ligne) : préciser si c'est le
    cas en cochant la case
-   Séparateur de champ : souvent c'est « ; », mais parfois c'est « , »
    ou tabulation
-   Type de guillemets : laisser celui qui est proposé
-   Encodage : ici utf-8
-   En cas d'erreur entraînant un abandon de l'import, aller dans les
    réglages avancés et choisir l'option « ignorer l'enregistrement »

![fenetre2](images/fenetre_config2.png){width=50%}

2. « Parcourir les données » : permet de vérifier que les données sont bien importées dans la table.  
Nous verrons ultérieurement qu’il y a des contraintes à définir et respecter sur les données. Dans les tables fournies, ces contraintes sont déjà vérifiées.

3. Vocabulaire.  
Une table de données est une **relation**, les colonnes sont les **attributs** de la relation.  
Exemple : dans la relation *films* précédente, l’attribut *runtime* donne la durée des films  


## Exploitation des données avec une seule table


### Projection

La projection est la sélection de certaines colonnes d'une table.

**SELECT** <liste d'attribut(s)> **FROM** <liste de table(s)>`

Les mots-clés peuvent être en majuscules ou en minuscules, l'usage fait
qu'on les tape en majuscules en général. On finit les lignes par un
point-virgule.

!!! example "**A tester**"
    Dans l'onglet « Exécuter le SQL »   
    Taper `SELECT title FROM films`  
    Exécuter avec le bouton « lecture » (la flèche bleue)

    ![execute](images/execute_SQL.png){width=70%}

    S'il y a plusieurs tables avec les mêmes noms d'attributs :

    ```sql
    SELECT films.title
    FROM films;
    ```

    Sélectionner plusieurs attributs :

    ```sql
    SELECT title, release_year
    FROM films;
    ```

    Supprimer les doublons :

    ```sql
    SELECT DISTINCT release_year
    FROM films;
    ```

    Sélectionner toutes les colonnes :

    ```sql
    SELECT * 
    FROM films;
    ```

### Ordonner les résultats

!!! example "**A tester**"
    ```sql
    SELECT title, release_year 
    FROM films 
    ORDER BY release_year ;
    
    SELECT title, release_year 
    FROM films 
    ORDER BY release_year DESC ;
    
    SELECT title, release_year 
    FROM films 
    ORDER BY release_year DESC , title ASC ;
    ```
    Par défaut, le tri est ascendant (dans l'ordre numérique ou lexicographique croissant)

### Restriction et sélection

L'objectif est d'afficher certaines lignes, qui vérifient un critère
donné.  
On utilise le mot-clé **`WHERE`**.

!!! example "**A tester**"
    ```sql
    SELECT title 
    FROM films 
    WHERE release_year = 1953 ;

    SELECT * 
    FROM films 
    WHERE title = "Batman" ;
    ```

### Opérateurs

On dispose des opérateurs de comparaison usuels, qui fonctionnent aussi bien avec les données numériques, les chaînes, ou les dates (que nous verrons ultérieurement).

Égalité **=** Différence **<>** Supérieur **>**Inférieur **<**

Supérieur ou égal **>=** Inférieur ou égal **<=**

L'opérateur **IN** permet de tester l'appartenance à une liste.

```sql
SELECT title 
FROM films 
WHERE release_year IN (1953, 1963, 1973) ;
```

*Complément :* sélectionner des lignes qui commencent (ou contiennent ou
finissent) par un caractère donné :

```sql
SELECT * 
FROM films 
WHERE title **LIKE** "ab%" ;
```

Donner la requête qui permet de trouver les films finissant par « ab »,
ainsi que ceux contenant « ab » :

!!! question "Exercice 1"
    Pour chacune de ces questions, vous insérerez vos requêtes et les
    résultats obtenus à la suite.  
    Il peut être utile d'aller dans l'onglet « Explorer les données » pour
    retrouver les noms des attributs.

    1. Donner la liste des titres et recettes des films ayant réalisé plus
    de 100 000 000 $ de recettes.

    2. Donner la liste de toutes les données sur les films réalisés entre
    1918 et 1939.  
    On proposera deux requêtes, la deuxième utilisera **BETWEEN **.  
    
    3. Donner la liste des films de langue anglaise, française, espagnole ou
    allemande.

    4. Donner la liste de toutes les langues différentes de réalisation.

    5. Donner la liste des titres et des langues des films qui n'ont pas été
    tournés en anglais (« en » pour english), en les classant par langue.

    6. Donner la liste des titres, année de sortie et durée des films de
    plus de 3 heures, classée par durée décroissante.

    7. Donner la liste des titres et années de sortie des films d'action. 

    8. Donner la liste des titres et années de sortie des films d'action
    dont le titre comporte « Man ».

### Attirubts calculés

On peut créer de nouvelles colonnes, en calculant un résultat à partir
d'une colonne existante.

*Remarquer la différence entre les deux requêtes suivantes*

```sql
SELECT title, revenue/1000000 
FROM films 
WHERE revenue > 1000000000;

SELECT title, revenue/1000000.0 
FROM films 
WHERE revenue > 1000000000;
```

Pour plus de clarté, on peut renommer une colonne avec **AS** :

```sql
SELECT title, revenue/1000000 AS recette_en_m$ 
FROM films 
WHERE revenue > 1000000000;
```

On peut créer de nouvelles colonnes à partir de colonnes existantes :

```sql
SELECT title, revenue/runtime AS recette_par_min 
FROM films 
WHERE revenue > 1000000000;
```

Ces fonctions servent à calculer une valeur à partir de plusieurs
lignes :

-   Somme **SUM**, moyenne **AVG**, minimum **MIN**, Maximum **MAX**
-   Compter un nombre de lignes : **COUNT**

!!! example
    ```sql
    SELECT AVG(revenue) AS recette_moyenne 
    FROM films;
    
    SELECT COUNT(*) AS nb_films, AVG(revenue) AS recette_moyenne, MIN(revenue) AS recette_min, MAX(revenue) AS recette_max 
    FROM films;
    ```

!!! info "Remarque :"
    Le dernier exemple est une requête assez longue, qui est peu lisible sous cette forme.

    On peut l'écrire sur plusieurs lignes :

    ```sql
    SELECT COUNT(*) AS nb_films,
    AVG(revenue) AS recette_moyenne,
    MIN(revenue) AS recette_min,
    MAX(revenue) AS recette_max
    FROM films;
    ```

*Tester et concluer  :*
```sql
SELECT title , COUNT(*) as nb_films 
FROM films
```

On peut combiner ces opérations de calcul avec une sélection :

```sql
SELECT COUNT(*) as nb_films_fr
FROM films
WHERE original_language = "fr" ;

SELECT COUNT(DISTINCT original_language) as nb_langues_vieux  
FROM films  
WHERE release_year < 1960;
```