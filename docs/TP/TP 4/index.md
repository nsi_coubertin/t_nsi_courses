___
## Jointures (exploitation des données avec plusieurs tables)

Importer les tables realisateurs.csv et distribution.csv dans la base de donnée actuelle (n’en créer pas une nouvelle).  
Les jointures sont utilisées pour combiner les informations de plusieurs tables, à l’aide des clés étrangères.  
Plusieurs rédactions sont possibles, en exemple ici pour donner le réalisateur de chaque film :

```sql
SELECT title , director
FROM films , realisateurs 
WHERE films.movie_id = realisateurs.movie_id;
```

```sql
SELECT title , director
FROM films
INNER JOIN realisateurs ON films.movie_id = realisateurs.movie_id;
```

```sql
SELECT films.title , realisateurs.director
FROM films
INNER JOIN realisateurs ON films.movie_id = realisateurs.movie_id;
```

!!! info "Remarques :"
    - La troisième rédaction a la préférence de votre professeur.  
        ◦ Par rapport à la première, elle a l’avantage de bien distinguer les tables des attributs et de différencier le mot-clé WHERE du mot-clé ON. Cette première pratique est obsolète. Par ailleurs, l’usage du mot-clé JOIN dans les deux autres rédactions permet de préciser le type de jointure. En terminale, on ne fait que des jointures internes, mais dans le TD de 1ère vous avez fait d’autres types de jointures.  
        ◦ Par rapport à la deuxième, la troisième rédaction respecte l’usage qui est de préciser la table des attributs à utiliser, ce qui se fait lorsqu’il y a plusieurs tables en jeu.
    - Dans les deux rédactions, on précise bien sur quel attribut on fait la jointure, avec en plus l’identifiant de la relation : `films.movie_id` est l’attribut movie_id de la relation films. Il est possible que les attributs aient des noms différents dans les tables, ce n’est pas un problème.
    - On peut utiliser les diverses opérations vues avant (et aussi en complément ci-après) lors d’une jointure :
        ```sql
        SELECT title , director 
        FROM films
        INNER JOIN realisateurs ON films.movie_id = realisateurs.movie_id 
        WHERE release_year = 1970;
        ```

!!! question "Exercice 3 :"
    Vérifier le nom des attributs avant de vous lancer dans les requêtes (fenêtre outils « DB Schema »).
    Respecter également scrupuleusement les orthographes des noms donnés ci-dessous.

    1. Donner la liste des acteurs ayant tourné dans des films français. La requête renverra le nom de l’acteur et le titre du film.
    2. Donner la liste des films dans lesquels a tourné Sigourney Weaver, classée par année de sortie.
    3. Donner la liste des acteurs du film "Amélie" (titre anglais de "Amélie Poulain").
    4. Donner la liste des acteurs ayant tourné sous la direction de Ridley Scott.
    5. Donner la liste par ordre alphabétique des acteurs dont les films comiques ont fait plus de 200 000 000 $ de recettes.
    6. Donner la liste des acteurs ayant tourné sous la direction de Ridley Scott, ainsi que les titres des films dans lesquels ils ont tourné.
    7. Donner la liste des acteurs ayant tourné sous la direction de Wong Kar-wai dans des films en Cantonais (l’abréviation pour Cantonais est… zh…)

       
## Créer une relation

L’objectif de cette partie est de créer une nouvelle relation (table), afin de pouvoir enregistrer les films vus.
           
**a. Exemples de création lors de l'import des données**

Dans l’onglet « Structure de la base de données », sélectionner la table `films`, puis cliquer sur « Modifier une table ».

Le code SQL de la création de la table 'films' s’affiche dans la fenêtre qui s’ouvre : 
```sql
CREATE TABLE "films" (
	"movie_id"	INTEGER,
	"title"	TEXT,
	"release_year"	INTEGER,
	"runtime"	INTEGER,
	"original_language"	TEXT,
	"genres"	TEXT,
	"revenue"	INTEGER
);
```

Cette requête a été exécutée avant l'importation des données du fichier `films.csv`  
Certaines contraintes d'intégrité n'ont pas été définies avant l'insertion des données.  
Dans la partie interface graphique de cette fenêtre, cocher les cases permettant de mieux construire la table, et observer les modifications du code.

!!! info inline end "Quelques remarques :"
    - SQL n’est pas sensible à la casse. Si cela vous amuse, vous pouvez écrire `iDeNtIfIaNt`, SQL reconnaîtra `identifiant`.  
    - Pour détruire une table, la requête est `DROP TABLE nom_de_la_table`. On peut rajouter `IF EXISTS` qui permet de ne pas renvoyer d’erreur si la table n’existe pas.

La requête SQL propre pour la relation `films` est :

```sql
CREATE TABLE `films` (
	`movie_id`    INTEGER NOT NULL UNIQUE,	--unicité et existence d'une référence du film
	`title`   TEXT,
	`release_year`    INTEGER,
	`runtime`    INTEGER,
	`original_language`    TEXT,
	`genres`    TEXT,
	`revenue`    INTEGER,
	PRIMARY KEY(`movie_id`)	  --définition de la clé primaire
);
```

Pour les autres tables, on précisera cela ainsi :
```sql
CREATE TABLE distribution (
	movie_id	INTEGER,
	actor    TEXT	--définition impossible d'une clé primaire
);

CREATE TABLE realisateurs (
	movie_id	INTEGER,
	director	TEXT,
	PRIMARY KEY(movie_id)
);
```


**b. Création d'une table `films_vus`**

Créer avec une requête SQL, et non avec l’interface graphique, la table `films_vu` dont les attributs sont :

- `movie_id`, non nul . C’est une clé étrangère : on rajoute en fin de la requête
```sql
FOREIGN KEY(`movie_id`) REFERENCES `films`(`movie_id`) 
```
- `date`. Le type est DATE  non nulle
- La clé primaire est le couple `movie_id`, `date` (on peut voir plusieurs fois le film)
- `note`. La note est un entier compris 1 et 5.
```sql
Note INTEGER CHECK ((note >= 1) AND (note <=5))
```
      
Vérifier que la table est bien créée, et que le code de création en est correct.


## Insertion / modification / suppression

**a. Pour rajouter des enregistrements, on utilise**

```sql
INSERT INTO table (liste d’attributs facultative)
VALUES liste de valeurs
```

!!! example "Exemples :"
    - Si les données sont dans l’ordre des champs :
    ```sql
    INSERT INTO films_vus VALUES (348, '12-sept-1979', 5);
    ```
    - Si les données sont dans le désordre ou manquantes :
    ```sql
    INSERT INTO films_vus ( "movie_id", "note", "date") VALUES (78, 5,"15-sept-1982");
    ```
    - Tester, corriger et commenter :
    ```sql
    INSERT INTO films_vus VALUES (348, '12-sept-1979', 5);
    ```


!!! question "Exercice 4 : "
    1. Dans la table `films_vus`, insérer 10 lignes de votre choix, dont au moins deux avec des notes inférieures ou égales à 2.  
    Mettre un ou plusieurs films sans « note ».
    2. Tester la requête 
    ```sql
    SELECT movie_id FROM films_vus WHERE note = NULL
    ```
    3. Tester la requête 
    ```sql
    SELECT movie_id FROM films_vus WHERE note IS NULL
    ```
    4. Conclure, et compter le nombre de films vus où la note n’est pas renseignée.
    5. Insérer 5 nouveaux films récents dans la table `films`.


**b. Pour supprimer des enregistrements, on utilise**

```sql
DELETE FROM table
WHERE condition
```

!!! question "Exercice 5 :"  
    1. Dans la table `films_vus`, supprimer les films vus dont la note est inférieure ou égale à 2.
    2. Dans la table `films`, supprimer les films dont la durée est anormalement petite.


**c. Pour mettre à jour des enregistrements, on utilise**

```sql
UPDATE table 
SET att1 = val1 , att2 = val2,… 
WHERE condition   --facultatif
```

!!! question "Exercice 6 :"
    Mettre à jour les enregistrements où la note n’est pas renseignée en mettant la valeur de votre choix.


## Vérification de la cohérence des données; améliorations

!!! question "Bilan :"
    1. Répondre aux questions suivantes : Y-a-t-il
        - des identifiants de films identiques présents plusieurs fois dans la table des films ?  
        - des couples acteurs/identifiants de films présents plusieurs fois dans la table des acteurs ?  
        - idem avec la relation des réalisateurs.  
        - des films sans réalisateurs ?  
        - des films sans acteurs ?  
        - des acteurs dans des films non référencés ?  
        - idem avec la relation des réalisateurs.  

    2. Utiliser ces résultats, ainsi qu'une critique des requêtes `CREATE`, pour répondre à la question : quels sont les qualités et défauts des relations films, distribution et réalisateurs, d'un point de vue purement "conception de la base de données" ?  
    3. Qu'auriez-vous proposé pour éviter ces défauts ?  
    *Vous pouvez aussi répondre à cette question en vous plaçant du côté utilisateur, qui n’a pas forcément les mêmes idées et besoins.*
