# -*- coding: utf-8 -*-
'''
:Titre : Implémentation d'une pile, version 1 : POO
:Auteur : L. Conoir
:Date : 10/2020
'''

class Pile(object):        # en anglais : Stack
    
    def __init__(self, capacite):
        'Constructeur de la classe'
        self.__elements = []
        self.__capacite = capacite
    
    
    def Est_Vide(self):
        'Méthode publique, précise si la pile est vide'
        return len(self.__elements) == 0
            
    
    def Est_Pleine(self):
        'Méthode publique, précise si la pile est pleine'
        return len(self.__elements) == self.__capacite
    
    
    def Empiler(self, element):
        'Méthode publique, empile un nouvel element.'
        if not self.Est_Pleine():
            self.__elements = [element] + self.__elements
        else:
            print('MI1 : La pile est pleine.')    # Stack Overflow
    
    def Depiler(self, pile):
        'Méthode publique, dépile le sommet et le renvoie.'
        if not self.Est_Vide :
            return self.__elements.pop(0)
        else:
            print('MI2 : La pile est vide.')
    
    
    def Nombre_Elements(self):
        "Méthode publique, renvoie le nombre d'éléments dans la pile."
        return len(self.__elements)
    
    
    def Capacite(self):
        'Méthode publique, renvoie la capacité de la pile.'
        return self.__capacite


#################################################################################
class Pile_bis(object):
    
    def __init__(self, capacite):
        self.__sommet = None
        self.__suite = []
        self.__capacite = capacite
        self.__nb_elements = 0
        
    def Est_Vide(self):
        return self.__nb_elements == 0
    
    def Est_Pleine(self):
        return len(self.__nb_elements) == self.__capacite
        
    def Empiler(self, element):
        if not self.Est_Pleine() :
            self.__suite.append(self.__sommet)
            self.__sommet = element
            self.__nb_elements += 1
        else :
            print('MI1 : La pile est pleine.')
            
    def Depiler(self):
        if not self.Est_Vide() :
            element = self.__sommet
            self.__nb_elements -= 1
            if self.__suite != [] :
                self.__sommet = self.__suite.pop()
            else :
                self.__sommet = None
            return element
        else :
            print('MI2 : La pile est vide.')
        
    def Nombre_Element(self):
        return self.__nb_element
    
    def Capacite(self):
        return self.__capcite
