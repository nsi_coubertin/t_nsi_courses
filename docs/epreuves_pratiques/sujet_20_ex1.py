# 1ère solution : recherche indicielle
def mini(releve, date):
    indice_mini = 0
    
    for indice in range (1,len(releve)):
        if releve[indice] < releve[indice_mini]:
            indice_mini = indice
        
    return releve[indice_mini], date[indice_mini]


# 2è solution : avec fonction min()
def mini2(releve, date):
    indice_mini = 0
    valeur_mini = min(releve)
    
    while releve[indice_mini] != valeur_mini:
        indice_mini = indice_mini + 1
    
    return releve[indice_mini], date[indice_mini]


# 3è solution : avec fonction min() et méthode index()
def mini3(releve,date):
    indice_mini = releve.index(min(releve))
    
    return releve[indice_mini], date[indice_mini]


t_moy = [14.9, 13.3, 13.1, 12.5, 13.0, 13.6, 13.7]
annees = [2013, 2014, 2015, 2016, 2017, 2018, 2019]
print('mini(t_moy, annees)')
print(mini(t_moy, annees))
print()
print('mini2(t_moy, annees)')
print(mini2(t_moy, annees))
print()
print('mini3(t_moy, annees)')
print(mini3(t_moy, annees))