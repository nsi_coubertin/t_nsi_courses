def inverse_chaine(chaine):
    result = ''
    for caractere in chaine:
       result = caractere + result
    return result

def est_palindrome(chaine):
    inverse = inverse_chaine(chaine)
    return chaine == inverse
    
def est_nbre_palindrome(nbre):
    chaine = str(nbre)
    return est_palindrome(chaine)

    
print("inverse_chaine('bac')")
print(inverse_chaine('bac'))
print()

print("est_palindrome('NSI')")
print(est_palindrome('NSI'))
print()

print("est_palindrome('ISN-NSI')")
print(est_palindrome('ISN-NSI'))
print()

print("est_nbre_palindrome(214312)")
print(est_nbre_palindrome(214312))
print()

print('est_nbre_palindrome(213312)')
print(est_nbre_palindrome(213312))
print()


################################################################################
## Remarques
################################################################################

#1#  La fonction est_palindrome pourrait également traiter le cas des nombres :
def est_palindrome2(chaine_ou_nombre):
    inverse = inverse_chaine(str(chaine_ou_nombre))
    return str(chaine_ou_nombre) == inverse

print("est_palindrome2('NSI')")
print(est_palindrome2('NSI'))
print()

print("est_palindrome2('ISN-NSI')")
print(est_palindrome2('ISN-NSI'))
print()

print("est_palindrome2(214312)")
print(est_palindrome2(214312))
print()

print('est_palindrome2(213312)')
print(est_palindrome2(213312))
print()

#2#  La fonction est_palindrome pourrait être récursive :
def est_palindrome3(chaine):
    if len(chaine) < 2 :
        return True
    else :
        if chaine[0] == chaine[-1] :
            return est_palindrome3(chaine[1:-1])
        else :
            return False

print("est_palindrome3('NSI')")
print(est_palindrome3('NSI'))
print()

print("est_palindrome3('ISN-NSI')")
print(est_palindrome3('ISN-NSI'))
print()

